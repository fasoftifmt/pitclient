# Para configurar o ambiente realize os passos a seguir: #

- Instale o NodeJs
[Download NodeJs](https://nodejs.org/en/)

- Instale o GIT 
[Download GIT](https://git-scm.com/download/win)

- Faça o clone do projeto
    * Acesse o repositório do projeto e clique em clone.
     ![passo01.PNG](https://bitbucket.org/repo/qzzeRE6/images/4039458636-passo01.PNG)
    * Certifique-se que esteja marcado HTTPS e Copie o comando.
     ![passo02.PNG](https://bitbucket.org/repo/qzzeRE6/images/3293552123-passo02.PNG)
- Abra o console do GIT BASH e navegue até a pasta onde deseja que fique o projeto
    * Cole o comando e execute, será iniciado o clone do projeto aguarde...

- Após o clone ser concluído, acesse a página do projeto.
- Execute o comando: ```npm install ```
- Execute o comando: ```npm install -g angular ```
- Execute o comando: ```npm install -g @angular/cli ```
- Para iniciar a aplicação execute o comando: ```ng serve```
- Para o build em diferentes ambientes ```ng build --configuration hom```
- Para o build alterando o href utilize o comando ```ng build --configuration test --base-href=/test/pit-ifmt/```
