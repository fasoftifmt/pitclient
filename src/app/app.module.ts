import {NgModule} from '@angular/core';
import {AppComponent} from './view/main/app.component';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './view/home/home.component';
import {ROUTES} from './app.routes';
import {RodapeComponent} from './view/rodape/rodape.component';
import {TopoComponent} from './view/topo/topo.component';
import {SharedModule} from './shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {UsuarioService} from './servicos/usuario.service';
import {GestaoPitComponent} from './view/gestao-pit/gestao-pit/gestao-pit.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        RodapeComponent,
        TopoComponent,
        GestaoPitComponent,
    ],
    providers: [
        UsuarioService,
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    imports: [
        BrowserAnimationsModule,
        SharedModule,
        RouterModule.forRoot(ROUTES)
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
