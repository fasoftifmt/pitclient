import {Routes} from '@angular/router';
import {HomeComponent} from './view/home/home.component';
import {LoggedInGuard} from './security/loggedin.guard';
import {LoginComponent} from './security/login/login.component';
import {PerfilGuard} from './security/perfil.guard';
import {NaoAutorizadoComponent} from './security/nao-autorizado/nao-autorizado.component';
import {GestaoPitComponent} from './view/gestao-pit/gestao-pit/gestao-pit.component';
import {PerfilUsuarioModel} from './models/enums/perfil-usuario.enum';

export const ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent,
        canLoad: [LoggedInGuard],
        data: [PerfilUsuarioModel.SERVIDOR],
        canActivate: [LoggedInGuard, PerfilGuard]
    },
    {path: 'login/:to', component: LoginComponent},
    {path: 'login', component: LoginComponent},
    {path: 'naoAutorizado', component: NaoAutorizadoComponent},

    {
        path: 'pit-view/:estado', loadChildren: './view/pit-view/pit.module#PitModule',
        data: [PerfilUsuarioModel.SERVIDOR],
        canLoad: [LoggedInGuard, PerfilGuard], canActivate: [LoggedInGuard, PerfilGuard]
    },
    {
        path: 'gestao', loadChildren: './view/gestao/gestao.module#GestaoModule',
        data: [PerfilUsuarioModel.ADMIN],
        canLoad: [LoggedInGuard, PerfilGuard], canActivate: [LoggedInGuard, PerfilGuard]
    },
    {
        path: 'gestao-pit',
        component: GestaoPitComponent,
        data: [PerfilUsuarioModel.GESTOR],
        canLoad: [LoggedInGuard, PerfilGuard], canActivate: [LoggedInGuard, PerfilGuard]
    }
];
