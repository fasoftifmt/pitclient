import {environment} from '../../environments/environment';

export const API_CONFIG = {
    baseUrl: environment.url_api,
    gravatarUrl: environment.url_gravatar
};
