export class AnexoPit {

    nome: string;
    descricao: string;
    tipo: string;
    docBase64: string;

    constructor(nome: string, descricao: string, tipo: string, docBase64: string) {
        this.nome = nome;
        this.descricao = descricao;
        this.tipo = tipo;
        this.docBase64 = docBase64;
    }
}
