import {Atividade} from './atividade.model';

export class AtividadePit {

    id: number;
    quantidade: number;
    atividade: Atividade;
    qtdLancamento: number;
    fator: number;

    constructor(id: number, quantidade: number, atividade: Atividade, qtdLancamento: number, fator: number) {
        this.id = id;
        this.quantidade = quantidade;
        this.atividade = atividade;
        this.qtdLancamento = qtdLancamento;
        this.fator = fator;
    }
}
