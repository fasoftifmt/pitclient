import {UnidadeAtividadeModel} from './enums/unidade-atividade.enum';
import {Categoria} from './categoria.model';

export class Atividade {

    id: number;
    descricao: string;
    qtdMax: number;
    dicaPreenchimento: string;
    unidadeAtividade: UnidadeAtividadeModel;
    permiteLancamentoMultiplos: boolean;
    qtdLancamento: number;
    fator: number;
    quantidade: number;
    cargaHorariaObtida: number;
    isSubAtividade: boolean;
    subAtividadeList: Atividade[];
    categoria: Categoria;
    atividadePertencente: number;

    constructor(
        id: number,
        descricao: string,
        qtdMax: number,
        dicaPreenchimento: string,
        fator: number,
        unidadeAtividade: UnidadeAtividadeModel,
        quantidade: number,
        cargaHorariaObtida: number,
        permiteLancamentoMultiplos: boolean,
        qtdLancamento: number
    ) {
        this.id = id;
        this.descricao = descricao;
        this.qtdMax = qtdMax;
        this.dicaPreenchimento = dicaPreenchimento;
        this.fator = fator;
        this.unidadeAtividade = unidadeAtividade;
        this.quantidade = quantidade;
        this.cargaHorariaObtida = cargaHorariaObtida;
        this.qtdLancamento = qtdLancamento;
        this.permiteLancamentoMultiplos = permiteLancamentoMultiplos;
    }

    public getLancamentos(): Array<number> {
        const lancamentos = [];
        for (let count = 1; count <= this.qtdMax; count++) {
            lancamentos.push(count);
        }
        return lancamentos;
    }
}
