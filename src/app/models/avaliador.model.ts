import {Servidor} from './servidor.model';
import {Campus} from './campus.model';

export class Avaliador {

    idAvaliador: number;
    descricao: string;
    servidor: Servidor;
    campus: Campus;

    constructor(descricao: string, servidor: Servidor, campus: Campus) {
            this.descricao = descricao;
            this.servidor = servidor;
            this.campus = campus;
    }
}
