import {GrupoAtividade} from './grupo-atividade.model';

export class CategoriaAtividade {

    id: number;
    descricao: string;
    ordem: number;
    grupoAtividade: GrupoAtividade;
    constructor(descricao: string, ordem: number, grupoAtividade: GrupoAtividade) {
        this.descricao = descricao;
        this.ordem = ordem;
        this.grupoAtividade = grupoAtividade;
    }
}
