import {Atividade} from './atividade.model';
import {NumberUtil} from '../utils/number.util';

export class Categoria {

    id: string;
    descricao: string;
    private atividadeList = new Array<Atividade>();
    private contemAtividadesComLancamentoMultiplo: boolean;
    private contemAtividadesComSubAtividades = false;
    private contemSubAtividades = false;

    constructor(id: string, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }

    public isAtividadesComLancamentoMultiplo(): boolean {
        return this.contemAtividadesComLancamentoMultiplo;
    }

    public contemAtividadesComSubAtividade(): boolean {
        return this.contemAtividadesComSubAtividades;
    }

    public contemSubAtividade(): boolean {
        return this.contemSubAtividades;
    }

    public setAtividadeList(atividades: Array<Atividade>): void {
        this.atividadeList = atividades;
        this.verificarAtividadesComLancamentoMultiplo();
        this.verificarSubAtividades();
    }

    public getAtividadesList(): Array<Atividade> {
        return this.atividadeList;
    }

    public getSubtotal(): number {
        let subTotal = 0;
        this.atividadeList.forEach((atv) => {
            subTotal += atv.cargaHorariaObtida;
        });
        return NumberUtil.arredondar(subTotal);
    }

    private verificarAtividadesComLancamentoMultiplo(): void {
        for (let i = 0; i < this.atividadeList.length; i++) {
            if (this.atividadeList[i].permiteLancamentoMultiplos) {
                this.contemAtividadesComLancamentoMultiplo = true;
                break;
            } else {
                this.contemAtividadesComLancamentoMultiplo = false;
            }
        }
    }

    private verificarSubAtividades(): void {
        for (let i = 0; i < this.atividadeList.length; i++) {
            if (this.atividadeList[i].isSubAtividade) {
                this.contemSubAtividades = true;
            }

            if (this.atividadeList[i].subAtividadeList !== undefined && this.atividadeList[i].subAtividadeList.length > 0) {
                this.contemAtividadesComSubAtividades = true;
                break;
            }
        }
    }
}
