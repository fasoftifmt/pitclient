import {Departamento} from 'src/app/models/departamento.model';
import {Disciplina} from './disciplina.model';

export class Curso {

    id: number;
    descricao: string;
    disciplinas: Array<Disciplina>;
    departamentoArea: Departamento;

    constructor(descricao: string) {

        this.descricao = descricao;
        //this.disciplinas = disciplina;
    }
}
