import {Campus} from './campus.model';

export class Departamento {

    id: number;
    descricao: string;
    sigla: string;
    campus: Campus;

    constructor(descricao: string, sigla: string, campus: Campus) {
        this.descricao = descricao;
        this.sigla = sigla;
        this.campus = campus;
    }
}
