import {Curso} from './curso.model';

export class Disciplina {

    id: number;
    descricao: string;
    cargaHoraria: number;
    numAulaSemanal: number;
    periodo: number;
    curso: Curso;

    constructor(id: number, descricao: string, cargaHoraria: number, numAulaSemanal: number, periodo: number) {
        this.id = id;
        this.descricao = descricao;
        this.cargaHoraria = cargaHoraria;
        this.numAulaSemanal = numAulaSemanal;
        this.periodo = periodo;
    }
}
