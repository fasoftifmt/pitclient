import {SubAtividade} from '../enums/sub-atividade.model';

export class AtividadeDTO {

    id: number;
    descricao: string;
    qtdMax: number;
    dicaPreenchimento: string;
    unidadeAtividade: string;
    permiteLancamentoMultiplos: boolean;
    fator: number;
    quantidade: number;
    // cargaHorariaObtida: number; calculo entre fator e qtde
    subAtividadeList: SubAtividade[];
    subAtividade: boolean;
    categoria: number;


    constructor(
        descricao: string,
        qtdMax: number,
        dicaPreenchimento: string,
        fator: number,
        unidadeAtividade: string,
        quantidade: number,
        //cargaHorariaObtida: number,
        permiteLancamentoMultiplos: boolean,
        subAtividade: boolean,
        categoria: number,
    ) {

        this.descricao = descricao;
        this.qtdMax = qtdMax;
        this.dicaPreenchimento = dicaPreenchimento;
        this.fator = fator;
        this.unidadeAtividade = unidadeAtividade;
        this.quantidade = quantidade;
        // this.cargaHorariaObtida = cargaHorariaObtida;
        this.permiteLancamentoMultiplos = permiteLancamentoMultiplos;
        this.subAtividade = subAtividade;
        this.categoria = categoria;

    }
}
