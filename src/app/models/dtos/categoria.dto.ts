import {AtividadeDTO} from './atividade.dto';

export class CategoriaDTO {

    id: string;
    descricao: string;
    atividadeModelList = new Array<AtividadeDTO>();

    constructor(id: string, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }
}
