export class DisciplinaDTO {

    id: number;
    descricao: string;
    cargaHoraria: number;
    numAulaSemanal: number;
    periodo: number;
    cursoId: number;

    constructor(id: number, descricao: string, cargaHoraria: number, numAulaSemanal: number, periodo: number, cursoId: number) {
        this.id = id;
        this.descricao = descricao;
        this.cargaHoraria = cargaHoraria;
        this.numAulaSemanal = numAulaSemanal;
        this.periodo = periodo;
        this.cursoId = cursoId;
    }
}
