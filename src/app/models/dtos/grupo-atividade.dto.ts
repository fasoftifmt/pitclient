import {CategoriaDTO} from './categoria.dto';

export class GrupoAtividadeDTO {

    id: number;
    descricao: string;
    categoriaAtividadeList = new Array<CategoriaDTO>();
    ordem: number;

    constructor(id: number, descricao: string) {
        this.id = id;
        this.descricao = descricao;
    }
}
