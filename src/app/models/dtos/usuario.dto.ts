export class UsuarioDTO {

    idUsuario: number;
    login: string;
    senha: string;
    perfilUsuarioSet: Array<number>;

    constructor(idUsuario: number, login: string, senha: string, perfilUsuarioSet: Array<number>) {
        this.idUsuario = idUsuario;
        this.login = login;
        this.senha = senha;
        this.perfilUsuarioSet = perfilUsuarioSet;
    }
}
