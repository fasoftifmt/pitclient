import {Enum} from './pit-enum.model';

export const enum SituacaoServidorModel {
    ATIVO = 'ATIVO',
    AFASTADO = 'AFASTADO',
    INATIVO = 'INATIVO',
}

export class SituacaoServidor extends Enum<SituacaoServidorModel> {
    static readonly ATIVO = new SituacaoServidor(1, 'Ativo', SituacaoServidorModel.ATIVO);
    static readonly AFASTADO = new SituacaoServidor(2, 'Afastado', SituacaoServidorModel.AFASTADO);
    static readonly INATIVO = new SituacaoServidor(3, 'Inativo', SituacaoServidorModel.INATIVO);

    constructor(codigo: number, descricao: string, name: SituacaoServidorModel) {
        super(codigo, descricao, name);
    }

    static getValues(): Enum<SituacaoServidorModel>[] {
        return [SituacaoServidor.ATIVO, SituacaoServidor.AFASTADO,
            SituacaoServidor.INATIVO];
    }

    static toEnum(value: string | number): Enum<SituacaoServidorModel> {
        switch (value) {
            case 1:
            case 'ATIVO':
                return SituacaoServidor.ATIVO;
            case 2:
            case 'AFASTADO':
                return SituacaoServidor.AFASTADO;
            case 3:
            case 'INATIVO':
                return SituacaoServidor.INATIVO;
            default:
                throw new Error('Nenhum enum correspontente para o valor: ' + value);
        }
    }
}
