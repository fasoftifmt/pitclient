export const enum GrupoRegenciaAula {
    VAZIO = '',
    GRUPO_01 = 'Docente com regime de trabalho de 20 (vinte) horas semanais',
    GRUPO_02 = 'Docente com regime de trabalho de 40 (quarenta) horas semanais',
    GRUPO_03 = 'Docente com regime de trabalho de 40 (quarenta) horas semanais em Dedicação Exclusiva',
    GRUPO_04 = 'Docente contratado em regime de 20 (vinte) horas semanais, conforme Lei 8.745/93',
    GRUPO_05 = 'Docente contratado em regime de 40 (quarenta) horas semanais, conforme Lei 8.745/93',
    GRUPO_06 = 'Docente que atua em Programa de Pós-Graduação strictu sensu ofertado pelo' +
        ' IFMT e com atividadePitList de pesquisa e/ou extensão regularmente registradas na instituição',
    GRUPO_07 = 'Docente com regime de 40 (quarenta) horas semanais com ou sem Dedicação Exclusiva,' +
        ' que desenvolve atividadePitList de pesquisa, e/ou extensão e/ou atuação em programas ' +
        'de Pós-Graduação latu sensu ofertados pelo IFMT'
}

export class Enum {

}
