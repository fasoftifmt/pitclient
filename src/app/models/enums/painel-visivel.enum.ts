export const enum Painel {
    ATIVIDADES = 'atividades',
    SERVIDOR = 'servidor',
    JUSTIFICATIVA = 'justificativa',
    ANEXO = 'anexo'
}
