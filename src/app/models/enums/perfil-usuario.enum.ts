import {Enum} from './pit-enum.model';

export enum PerfilUsuarioModel {
    SERVIDOR = 'SERVIDOR',
    ADMIN = 'ADMIN',
    GESTOR = 'GESTOR'
}

export class PerfilUsuario extends Enum<PerfilUsuarioModel> {
    static readonly SERVIDOR = new PerfilUsuario(1, 'SERVIDOR', PerfilUsuarioModel.SERVIDOR);
    static readonly ADMIN = new PerfilUsuario(2, 'ADMIN', PerfilUsuarioModel.ADMIN);
    static readonly GESTOR = new PerfilUsuario(3, 'GESTOR', PerfilUsuarioModel.GESTOR);

    constructor(codigo: number, descricao: string, name: PerfilUsuarioModel) {
        super(codigo, descricao, name);
    }

    static getValues(): Enum<PerfilUsuarioModel>[]  {
        return [PerfilUsuario.SERVIDOR, PerfilUsuario.ADMIN, PerfilUsuario.GESTOR];
    }
    public static toEnum(value: string | number ): Enum<PerfilUsuarioModel> {
        switch (value) {
            case 1:
            case 'SERVIDOR':
                return PerfilUsuario.SERVIDOR;
            case 2:
            case 'ADMIN':
                return PerfilUsuario.ADMIN;
            case 3:
            case 'GESTOR':
                return PerfilUsuario.GESTOR;
            default:
                throw new Error('Nenhum enum correspondente para o valor: ' + value);
        }
    }
}
