export interface EnumInterface<T> {

    getName(): T;

    getCodigo(): number;

    getDescricao(): string;

}

export abstract class Enum<T> implements EnumInterface<T> {

    private readonly codigo: number;
    private readonly descricao: string;
    private readonly name: T;

    protected constructor(codigo: number, descricao: string, name: T) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.name = name;
    }

    getCodigo(): number {
        return this.codigo;
    }

    getDescricao(): string {
        return this.descricao;
    }

    getName(): T {
        return this.name;
    }

    public toString(): string {
        return this.descricao;
    }
}
