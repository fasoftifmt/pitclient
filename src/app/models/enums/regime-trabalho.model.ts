import {Enum} from './pit-enum.model';

export const enum RegimeTrabalhoModel {
    VINTE_HORAS = 'VINTE_HORAS',
    QUARENTA_HORAS = 'QUARENTA_HORAS',
    DEDICACAO_EXCLUSIVA = 'DEDICACAO_EXCLUSIVA'
}

export class RegimeTrabalho extends Enum<RegimeTrabalhoModel> {

    static readonly VINTE_HORAS = new RegimeTrabalho(1, 'Vinte horas', RegimeTrabalhoModel.VINTE_HORAS);
    static readonly QUARENTA_HORAS = new RegimeTrabalho(2, 'Quarenta horas', RegimeTrabalhoModel.QUARENTA_HORAS);
    static readonly DEDICACAO_EXCLUSIVA = new RegimeTrabalho(3, 'Dedicação exclusiva', RegimeTrabalhoModel.DEDICACAO_EXCLUSIVA);

    constructor(codigo: number, descricao: string, name: RegimeTrabalhoModel) {
        super(codigo, descricao, name);
    }

    static getValues(): Enum<RegimeTrabalhoModel>[] {
        return [RegimeTrabalho.VINTE_HORAS, RegimeTrabalho.QUARENTA_HORAS, RegimeTrabalho.DEDICACAO_EXCLUSIVA];
    }

    static toEnum(value: string | number): Enum<RegimeTrabalhoModel> {
        switch (value) {
            case 1:
            case 'VINTE_HORAS':
                return RegimeTrabalho.VINTE_HORAS;
            case 2:
            case 'QUARENTA_HORAS':
                return RegimeTrabalho.QUARENTA_HORAS;
            case 3:
            case 'DEDICACAO_EXCLUSIVA':
                return RegimeTrabalho.DEDICACAO_EXCLUSIVA;
            default:
                throw new Error('Nenhum enum correspondente para o valor: ' + value);
        }
    }

}
