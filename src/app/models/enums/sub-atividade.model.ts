import {UnidadeAtividade} from './unidade-atividade.enum';

export class SubAtividade {

    id: number;
    fator: number;
    descricao: string;
    unidadeAtividade: UnidadeAtividade;

    constructor(id: number, fator: number, descricao: string, unidadeAtividade: UnidadeAtividade) {
        this.id = id;
        this.fator = fator;
        this.descricao = descricao;
        this.unidadeAtividade = unidadeAtividade;
    }
}
