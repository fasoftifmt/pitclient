import {Enum} from './pit-enum.model';

export const enum TipoDeVinculoModel {
    EFETIVO = 'EFETIVO',
    TEMPORARIO = 'TEMPORARIO',
    SUBSTITUTO = 'SUBSTITUTO',
    COLABORACAO_TECNICA = 'COLABORACAO_TECNICA'
}

export class TipoDeVinculo extends Enum<TipoDeVinculoModel> {

    static readonly EFETIVO = new TipoDeVinculo(1, 'Efetivo', TipoDeVinculoModel.EFETIVO);
    static readonly TEMPORARIO = new TipoDeVinculo(2, 'Temporario', TipoDeVinculoModel.TEMPORARIO);
    static readonly SUBSTITUTO = new TipoDeVinculo(3, 'Substituto', TipoDeVinculoModel.SUBSTITUTO);
    static readonly COLABORACAO_TECNICA = new TipoDeVinculo(4, 'Colaboração Tecnica', TipoDeVinculoModel.COLABORACAO_TECNICA);

    constructor(codigo: number, descricao: string, name: TipoDeVinculoModel) {
        super(codigo, descricao, name);
    }

    static getValues(): Enum<TipoDeVinculoModel>[] {
        return [TipoDeVinculo.EFETIVO, TipoDeVinculo.TEMPORARIO, TipoDeVinculo.SUBSTITUTO, TipoDeVinculo.COLABORACAO_TECNICA];
    }

    static toEnum(value: string | number): Enum<TipoDeVinculoModel> {
        switch (value) {
            case 1:
            case 'EFETIVO':
                return TipoDeVinculo.EFETIVO;
            case 2:
            case 'TEMPORARIO':
                return TipoDeVinculo.TEMPORARIO;
            case 3:
            case 'SUBSTITUTO':
                return TipoDeVinculo.SUBSTITUTO;
            case 4:
            case 'COLABORACAO_TECNICA':
                return TipoDeVinculo.COLABORACAO_TECNICA;
            default:
                throw new Error('Nenhum enum correspontente para o valor: ' + value);
        }
    }
}
