import {Enum} from './pit-enum.model';

export const enum UnidadeAtividadeModel {
    HORAS = 'HORAS',
    AULAS = 'AULAS'
}

export class UnidadeAtividade extends Enum<UnidadeAtividadeModel> {


    static readonly HORAS = new UnidadeAtividade(1, 'Horas', UnidadeAtividadeModel.HORAS);
    static readonly AULAS = new UnidadeAtividade(2, 'Aulas', UnidadeAtividadeModel.AULAS);


    constructor(codigo: number, descricao: string, name: UnidadeAtividadeModel) {
        super(codigo, descricao, name);
    }

    static getValues(): Enum<UnidadeAtividadeModel>[] {
        return [UnidadeAtividade.HORAS, UnidadeAtividade.AULAS];
    }

    static toEnum(value: string | number): Enum<UnidadeAtividadeModel> {
        switch (value) {
            case 1:
            case 'HORAS':
                return UnidadeAtividade.HORAS;
            case 2:
            case 'AULAS':
                return UnidadeAtividade.AULAS;
            default:
                throw new Error('Nenhum enum correspontente para o valor: ' + value);
        }
    }
}
