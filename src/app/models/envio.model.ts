export class Envio {

    private idEnvio: number;
    private dataEnvio: Date;
    private dataRetorno: Date;

    constructor(idEnvio: number, dataEnvio: Date, dataRetorno: Date) {
        this.idEnvio = idEnvio;
        this.dataEnvio = dataEnvio;
        this.dataRetorno = dataRetorno;
    }
}
