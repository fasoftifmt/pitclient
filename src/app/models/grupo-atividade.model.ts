import {Categoria} from './categoria.model';
import {Atividade} from './atividade.model';
import {NumberUtil} from '../utils/number.util';

export class GrupoAtividade {

    id: number;
    descricao: string;
    ordem: number;
    categoriaAtividadeList = new Array<Categoria>();

    constructor(descricao: string, ordem: number) {
        this.descricao = descricao;
        this.ordem = ordem;
    }

    public getCargaHorariaTotal(): number {
        let totalCargaHoraria = 0;
        this.categoriaAtividadeList.forEach((atv) => {
            totalCargaHoraria += atv.getSubtotal();
        });
        return NumberUtil.arredondar(totalCargaHoraria);
    }

    public getAtividadesComSubAtividades(): Atividade[] {
        const atividadesComSub = new Array<Atividade>();

        this.categoriaAtividadeList.forEach(categoria => {
            if (categoria.contemAtividadesComSubAtividade()) {
                categoria.getAtividadesList().forEach(atividade => {
                    if (atividade.subAtividadeList !== undefined && atividade.subAtividadeList.length > 0) {
                        atividadesComSub.push(atividade);
                    }
                });
            }
        });

        return atividadesComSub;
    }

}
