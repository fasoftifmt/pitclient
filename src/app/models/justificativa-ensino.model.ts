import {Disciplina} from './disciplina.model';

export class JustificativaEnsino {

    id: number;
    disciplina: Disciplina;
    curso: string;
    turma: string;
    aulas: string;

    constructor(id: number, curso: string, disciplina: Disciplina, turma: string, aulas: string) {
        this.id = id;
        this.disciplina = disciplina;
        this.turma = turma;
        this.aulas = aulas;
        this.curso = curso;
    }
}
