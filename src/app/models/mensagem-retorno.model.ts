export class MensagemRetorno {

    sucesso: boolean;
    msgList: Array<string>;

    constructor(sucesso: boolean, msgList: Array<string>) {
        this.sucesso = sucesso;
        this.msgList = msgList;
    }
}
