export class Pessoa {

    idServidor: number;
    nome: string;
    email: string;
    fone: string;

    constructor(idServidor: number, nome: string, email: string, fone: string) {
        this.idServidor = idServidor;
        this.nome = nome;
        this.email = email;
        this.fone = fone;
    }
}
