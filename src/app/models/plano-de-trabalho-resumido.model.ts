import {StatusPtd} from './enums/status-ptd.enum';

export class PlanoDeTrabalhoResumido {

    id: number;
    dataEnvio: Date;
    statusPtd: StatusPtd;

    constructor(id: number, dataEnvio: Date, statusPtd: StatusPtd) {
        this.id = id;
        this.dataEnvio = dataEnvio;
        this.statusPtd = statusPtd;
    }
}
