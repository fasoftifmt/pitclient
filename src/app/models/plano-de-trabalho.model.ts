import {Envio} from './envio.model';
import {Servidor} from './servidor.model';
import {StatusPtd} from './enums/status-ptd.enum';
import {AtividadePit} from './atividade-pit.model';
import {GrupoAtividade} from './grupo-atividade.model';
import {JustificativaEnsino} from './justificativa-ensino.model';
import {AnexoPit} from './anexo-pit.model';

export class PlanoDeTrabalho {

    id: number;
    dataElaboracao: string;
    statusPtd: StatusPtd;
    atividadePitList = new Array<AtividadePit>();
    justificativaEnsinoList = new Array<JustificativaEnsino>();
    gruposAtividades: Array<GrupoAtividade>;
    anexoPITList: Array<AnexoPit>;
    envio: Envio;
    servidor: Servidor;

    constructor(id: number, dataElaboracao: string, statusPtd: StatusPtd, servidor: Servidor) {
        this.id = id;
        this.dataElaboracao = dataElaboracao;
        this.statusPtd = statusPtd;
        this.servidor = servidor;
    }
}
