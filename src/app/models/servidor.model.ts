import {Pessoa} from './pessoa.model';
import {RegimeTrabalho} from './enums/regime-trabalho.model';
import {TipoDeVinculo} from './enums/tipo-de.vinculo';
import {SituacaoServidor} from './enums/SituacaoServidor';
import {Usuario} from './usuario.model';
import {Departamento} from './departamento.model';

export class Servidor extends Pessoa {

    regimeTrabalho: RegimeTrabalho;
    matriculaSiape: string;
    tipoDeVinculo: TipoDeVinculo;
    situacaoServidor: SituacaoServidor;
    usuario: Usuario;
    departamentoArea: Departamento;

    constructor(
        idServidor: number,
        nome: string,
        email: string,
        matriculaSiape: string,
        fone: string,
        departamentoArea: Departamento,
        regimeTrabalho: RegimeTrabalho,
        tipoDeVinculo: TipoDeVinculo,
        situacaoServidor: SituacaoServidor,
    ) {
        super(idServidor, nome, email, fone);
        this.regimeTrabalho = regimeTrabalho;
        this.matriculaSiape = matriculaSiape;
        this.tipoDeVinculo = tipoDeVinculo;
        this.situacaoServidor = situacaoServidor;
        this.departamentoArea = departamentoArea;
    }

    contemPermissao(perfilUsuario: string): boolean {
       const perfil = this.usuario.perfilUsuarioSet.find(value =>
            value.toString() === perfilUsuario);

        return perfil !== null && perfil !== undefined;
    }
}
