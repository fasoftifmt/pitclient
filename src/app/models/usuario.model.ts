import {PerfilUsuario, PerfilUsuarioModel} from './enums/perfil-usuario.enum';

export class Usuario {
    idUsuario: number;
    login: string;
    senha: string;
    perfilUsuarioSet: Array<PerfilUsuarioModel>;

    constructor(idUsuario: number, login: string, senha: string) {
        this.login = login;
        this.senha = senha;
        this.idUsuario = idUsuario;
    }
}
