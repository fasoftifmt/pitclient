import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from './storage.service';
import {UsuarioService} from '../servicos/usuario.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const storageService = this.injector.get(StorageService);
        const loginService = this.injector.get(UsuarioService);
        if (loginService.isLoggedIn()) {
            const authRequest = request.clone(
                {setHeaders: {'Authorization': `Bearer ${storageService.getLocalUser().token}`}});
            return next.handle(authRequest);
        } else {
            return next.handle(request);
        }
    }

}
