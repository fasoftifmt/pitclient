import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UsuarioService} from '../../servicos/usuario.service';
import {CredenciaisDto} from '../../models/credenciais.dto';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Md5} from 'ts-md5';
import {MessageService} from 'primeng/api';
import {Servidor} from '../../models/servidor.model';
import {PerfilUsuario} from '../../models/enums/perfil-usuario.enum';

@Component({
    selector: 'pit-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    navigateTo: string;

    constructor(
        private fb: FormBuilder,
        private loginService: UsuarioService,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageService,
        private router: Router) {
    }

    ngOnInit() {
        this.loginForm = this.fb.group({
            login: this.fb.control('', [Validators.required]),
            senha: this.fb.control('', [Validators.required]),
        });
        this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/');
    }

    login() {
        const senhaHashStr: string = Md5.hashStr(this.loginForm.value.senha) as string;
        const loginHashStr: string = Md5.hashStr(this.loginForm.value.login) as string;

        const creds: CredenciaisDto = {
            login: loginHashStr,
            senha: senhaHashStr
        };

        this.loginService.authenticate(creds)
            .subscribe((response: HttpResponse<Object>) => {
                    this.loginService.successFullLogin(response.headers.get('Authorization'));

                    this.loginService.getServidorPorLogin(this.loginService.getLocalUser().email)
                        .subscribe(servidor => {
                            this.loginService.setServidor(servidor);
                            this.loginService.redirecionarParaInicio();
                        });

                },
                (erro: HttpErrorResponse) => {
                    let msg = 'Erro desconhecido';

                    if (erro.status === 0) {
                        msg = 'Não foi possível conectar ao servidor';
                    }

                    if (erro.status === 403) {
                        msg = 'Dados de acesso incorretos!';
                        this.loginForm.controls['senha'].setErrors({'incorrect': true});
                        this.loginForm.controls['login'].setErrors({'incorrect': true});
                    }
                    this.messageService.add({
                        severity: 'error',
                        summary: 'Falha no login',
                        detail: msg
                    });
                }
            );
    }
}
