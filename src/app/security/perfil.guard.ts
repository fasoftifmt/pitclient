import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot} from '@angular/router';
import {UsuarioService} from '../servicos/usuario.service';
import {Observable} from 'rxjs';

export class PerfilGuard implements CanLoad, CanActivate {

    constructor(private loginService: UsuarioService) {
    }

    checkAuthentication(route: Route): Observable<boolean> {
        return this.loginService.hasPermission(route);
    }

    canLoad(route: Route): Observable<boolean> {
        return this.checkAuthentication(route);
    }

    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
        return this.checkAuthentication(activatedRoute.routeConfig);
    }
}
