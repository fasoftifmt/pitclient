import {Injectable} from '@angular/core';
import {AtividadeDTO} from '../models/dtos/atividade.dto';
import {Atividade} from '../models/atividade.model';
import {UnidadeAtividade} from '../models/enums/unidade-atividade.enum';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {Observable} from 'rxjs';
import {API_CONFIG} from '../config/api.config';
import {HttpClient} from '@angular/common/http';
import {MensagemRetorno} from '../models/mensagem-retorno.model';

@Injectable()
export class AtividadeService {


    constructor(public http: HttpClient) {

    }

    findAll(): Observable<Atividade[]> {
        return this.http.get<Atividade[]>(`${API_CONFIG.baseUrl}/atividades/buscarTodos`);
    }

    inserir(atividade: AtividadeDTO): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/atividades/inserir`, atividade);
    }

    public editar(atividade: AtividadeDTO): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/atividades`;
        return this.http.put<MensagemRetorno>(url, atividade);
    }


    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/atividades/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public vincularAtividades(ids: Array<number>): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/atividades/vincular`;
        return this.http.post<MensagemRetorno>(url, ids);

    }

    public desvincularAtividades(ids: Array<number>): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/atividades/desvincular`;
        return this.http.put<MensagemRetorno>(url, ids);

    }

    public fromDTO(atividadeDTO: AtividadeDTO): Atividade {

        const unidadeAtividade = atividadeDTO.unidadeAtividade.toString() === 'HORAS' ? UnidadeAtividade.HORAS : UnidadeAtividade.AULAS;

        const atividade = new Atividade(
            atividadeDTO.id,
            atividadeDTO.descricao,
            atividadeDTO.qtdMax,
            atividadeDTO.dicaPreenchimento,
            atividadeDTO.fator,
            unidadeAtividade.getName(),
            atividadeDTO.quantidade,
            null,
            atividadeDTO.permiteLancamentoMultiplos,
            undefined
        );

        // atividade.subAtividadeList = atividadeDTO.subAtividadeList;
        //  atividade.subAtividade = atividadeDTO.subAtividade;
        return atividade;
    }

    public fromDTOList(atividadeDTOList: AtividadeDTO[]): Atividade[] {
        const lista = new Array<Atividade>();
        atividadeDTOList.forEach((atvDTO) => {
            lista.push(this.fromDTO(atvDTO));
        });
        return lista;
    }


    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/atividades/${id}`);
    }
}
