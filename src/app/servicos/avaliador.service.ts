import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Avaliador} from '../models/avaliador.model';
import {API_CONFIG} from '../config/api.config';
import {MensagemRetorno} from '../models/mensagem-retorno.model';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {AvaliadorDTO} from '../models/dtos/avaliador.dto';

@Injectable()
export class AvaliadorServico {
    constructor(public http: HttpClient) {
    }
    findAll(): Observable<Avaliador[]> {
        return this.http.get<Avaliador[]>(`${API_CONFIG.baseUrl}/avaliador/buscarTodos`);
    }
    inserir(avaliador: Avaliador): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/avaliador/inserir`, avaliador);
    }
    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable <any> {
        let url = `${API_CONFIG.baseUrl}/avaliador/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public editar(ava: Avaliador): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/avaliador`;
        return this.http.put<MensagemRetorno>(url, ava);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/avaliador/${id}`);
    }
}
