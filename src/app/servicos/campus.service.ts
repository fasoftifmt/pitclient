import {HttpClient} from '@angular/common/http';
import {Campus} from '../models/campus.model';
import {MensagemRetorno} from '../models/mensagem-retorno.model';
import {Observable} from 'rxjs';
import {API_CONFIG} from '../config/api.config';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {Injectable} from '@angular/core';


@Injectable()
export class CampusService {

    constructor(private http: HttpClient) {
    }

    inserir(campus: Campus): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/campus/inserir`, campus);
    }

    public buscaPaginada(nome: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/campus/pagina?nome=${nome}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public listarTodos(): Observable<any> {

        const url = `${API_CONFIG.baseUrl}/campus/listarTodos`;

        return this.http.get<any>(url);
    }

    public editar(cmp: Campus): Observable<MensagemRetorno> {

        const url = `${API_CONFIG.baseUrl}/campus`;

        return this.http.put<MensagemRetorno>(url, cmp);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/campus/${id}`);
    }

}
