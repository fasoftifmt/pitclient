import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoriaAtividade} from '../models/categoria-atividade.model';
import {API_CONFIG} from '../config/api.config';
import {MensagemRetorno} from '../models/mensagem-retorno.model';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';


@Injectable()
export class CategoriaAtividadeServico {

    constructor(public http: HttpClient) {

    }

    findAll(): Observable<CategoriaAtividade[]> {
        return this.http.get<CategoriaAtividade[]>(`${API_CONFIG.baseUrl}/categoriaAtividades/buscarTodos`);
    }

    inserir(categoriaAtividade: CategoriaAtividade): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/categoriaAtividades/inserir`, categoriaAtividade);
    }

    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/categoriaAtividades/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public editar(catAt: CategoriaAtividade): Observable<MensagemRetorno> {

        const url = `${API_CONFIG.baseUrl}/categoriaAtividades`;

        return this.http.put<MensagemRetorno>(url, catAt);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/categoriaAtividades/${id}`);
    }

}
