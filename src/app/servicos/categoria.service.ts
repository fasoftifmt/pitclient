import {Injectable} from '@angular/core';
import {Categoria} from '../models/categoria.model';
import {CategoriaDTO} from '../models/dtos/categoria.dto';
import {AtividadeService} from './atividade.service';

@Injectable()
export class CategoriaService {


    constructor(private atividadeService: AtividadeService) {
    }

    public fromDTO(categoriaDTO: CategoriaDTO): Categoria {
        const categoria = new Categoria(categoriaDTO.id, categoriaDTO.descricao);
        categoria.setAtividadeList(this.atividadeService.fromDTOList(categoriaDTO.atividadeModelList));

        return categoria;
    }

    public fromDTOList(categoriaDTOList: Array<CategoriaDTO>): Categoria[] {
        const categoriaList = new Array<Categoria>();
        categoriaDTOList.forEach((catDTO) => {
            categoriaList.push(this.fromDTO(catDTO));
        });
        return categoriaList;
    }
}
