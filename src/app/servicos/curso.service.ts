import {CursoDTO} from './../models/dtos/curso.dto';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';
import {Observable} from 'rxjs';
import {Curso} from '../models/curso.model';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {MensagemRetorno} from '../models/mensagem-retorno.model';

@Injectable()
export class CursoService {

    constructor(public http: HttpClient) {
    }

    findAll(): Observable<Curso[]> {
        return this.http.get<Curso[]>(`${API_CONFIG.baseUrl}/cursos`);
    }

    inserir(curso: CursoDTO): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/cursos/inserir`, curso);
    }

    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/cursos/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public editar(curso: CursoDTO): Observable<MensagemRetorno> {

        const url = `${API_CONFIG.baseUrl}/cursos`;
        return this.http.put<MensagemRetorno>(url, curso);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/cursos/${id}`);
    }
}
