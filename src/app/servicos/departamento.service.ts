import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';
import {Observable} from 'rxjs';
import {Departamento} from '../models/departamento.model';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {MensagemRetorno} from '../models/mensagem-retorno.model';

@Injectable()
export class DepartamentoServico {

    constructor(public http: HttpClient) {
    }

    findAll(): Observable<Departamento[]> {
        return this.http.get<Departamento[]>(`${API_CONFIG.baseUrl}/departamentos/buscarTodos`);
    }

    inserir(departamento: Departamento): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/departamentos/inserir`, departamento);
    }

    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/departamentos/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public editar(dep: Departamento): Observable<MensagemRetorno> {

        const url = `${API_CONFIG.baseUrl}/departamentos`;

        return this.http.put<MensagemRetorno>(url, dep);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/departamentos/${id}`);
    }

}
