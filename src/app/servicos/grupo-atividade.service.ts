import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';
import {Observable} from 'rxjs';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {GrupoAtividade} from '../models/grupo-atividade.model';
import {CategoriaService} from './categoria.service';
import {MensagemRetorno} from '../models/mensagem-retorno.model';

@Injectable()
export class GrupoAtividadeService {

    constructor(private http: HttpClient, private categoriaService: CategoriaService) {
    }

    inserir(grupoAtividade: GrupoAtividade): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/grupoAtividades/inserir`, grupoAtividade);
    }

    public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

        let url = `${API_CONFIG.baseUrl}/grupoAtividades/pagina?descricao=${descricao}&page=${pagina}`;
        url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;

        return this.http.get<any>(url);
    }

    public editar(gra: GrupoAtividade): Observable<MensagemRetorno> {

        const url = `${API_CONFIG.baseUrl}/grupoAtividades`;

        return this.http.put<MensagemRetorno>(url, gra);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/grupoAtividades/${id}`);
    }

    public listarTodos(): Observable<any> {
        const url = `${API_CONFIG.baseUrl}/grupoAtividades/buscarTodos`;
        return this.http.get<any>(url);
    }

    public findAll(): Observable<GrupoAtividade[]> {
        return this.http.get<GrupoAtividade[]>(`${API_CONFIG.baseUrl}/grupoAtividades`);
    }

}
