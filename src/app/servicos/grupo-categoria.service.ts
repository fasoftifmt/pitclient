import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';
import {Observable} from 'rxjs';
import {GrupoAtividade} from '../models/grupo-atividade.model';
import {CategoriaService} from './categoria.service';
import {GrupoAtividadeDTO} from '../models/dtos/grupo-atividade.dto';

@Injectable()
export class GrupoCategoriaService {

    constructor(
        private http: HttpClient,
        private categoriaService: CategoriaService
    ) {
    }

    public findAll(): Observable<GrupoAtividadeDTO[]> {
        return this.http.get<GrupoAtividadeDTO[]>(`${API_CONFIG.baseUrl}/grupoAtividades/buscarTodos`);
    }

    public fromDTO(grupoAtividade: GrupoAtividadeDTO): GrupoAtividade {
        const grupoAtiv = new GrupoAtividade(grupoAtividade.descricao, grupoAtividade.ordem);
        grupoAtiv.categoriaAtividadeList = this.categoriaService.fromDTOList(grupoAtividade.categoriaAtividadeList);
        return grupoAtiv;
    }

    public fromDTOList(grupoAtividadeDTOList: GrupoAtividadeDTO[]): GrupoAtividade[] {
        const grupoAtividadeList = new Array<GrupoAtividade>();
        grupoAtividadeDTOList.forEach((grupoDTO) => {
            grupoAtividadeList.push(this.fromDTO(grupoDTO));
        });
        return grupoAtividadeList;
    }
}
