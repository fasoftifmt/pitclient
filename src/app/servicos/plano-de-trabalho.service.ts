import {HttpClient, HttpHeaders} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';
import {Observable} from 'rxjs';
import {PlanoDeTrabalho} from '../models/plano-de-trabalho.model';
import {Injectable} from '@angular/core';
import {PlanoDeTrabalhoResumido} from '../models/plano-de-trabalho-resumido.model';

@Injectable()
export class PlanoDeTrabalhoService {

    constructor(
        private http: HttpClient) {
    }

    public insert(obj: PlanoDeTrabalho): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'observe': 'response',
                'responseType': 'text',
            })
        };
        return this.http.post(
            `${API_CONFIG.baseUrl}/pit/novo`,
            JSON.stringify(obj),
            httpOptions
        );
    }

    public buscarPorDocente(servidorId: number): Observable<PlanoDeTrabalhoResumido[]> {
        return this.http.get<PlanoDeTrabalhoResumido[]>(`${API_CONFIG.baseUrl}/pit/buscarPits/${servidorId}`);
    }
}
