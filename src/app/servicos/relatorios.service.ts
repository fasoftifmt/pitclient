import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../config/api.config';

@Injectable({
    providedIn: 'root'
})
export class RelatoriosService {

    constructor(public http: HttpClient) {
    }

    relatorioDeServidor(): Promise<Blob> {
        return this.http.get<any>(`${API_CONFIG.baseUrl}/servidores/relatorios/servidores`,
            {responseType: 'blob' as 'json'})
            .toPromise();
    }
}
