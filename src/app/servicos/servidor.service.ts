import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MensagemRetorno} from '../models/mensagem-retorno.model';
import {HttpClient} from '@angular/common/http';
import {Servidor} from '../models/servidor.model';
import {API_CONFIG} from '../config/api.config';
import {DirecaoOrder} from '../models/enums/direcao-order.enum';
import {RegimeTrabalho} from '../models/enums/regime-trabalho.model';
import {TipoDeVinculo} from '../models/enums/tipo-de.vinculo';
import {SituacaoServidor} from '../models/enums/SituacaoServidor';

@Injectable()
export class ServidorService {

    constructor(public http: HttpClient) {
    }

    inserir(servidor: Servidor): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/servidores/inserir`, servidor);
    }

    public buscaPaginada(nome: string, pagina: number, linhaPorPagina: number,
                         ordenarPor: string, direcao: DirecaoOrder): Observable<any> {
        let url = `${API_CONFIG.baseUrl}/servidores/pagina?nome=${nome}&page=${pagina}`;
        url += `&linesPerPage=${linhaPorPagina}&orderBy=${ordenarPor}&direction=${direcao.toString()}`;
        return this.http.get<any>(url);
    }

    public editar(serv: Servidor): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/servidores`;
        return this.http.put<MensagemRetorno>(url, serv);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/servidores/${id}`);
    }
    public listarTodos(): Observable<any> {
        const url = `${API_CONFIG.baseUrl}/servidores/listarTodos`;
        return this.http.get<any>(url);
    }

    public fromDTO(servidorDTO: Servidor): Servidor {
        return new Servidor(servidorDTO.idServidor, servidorDTO.nome, servidorDTO.email,
            servidorDTO.matriculaSiape,
            servidorDTO.fone,
            servidorDTO.departamentoArea,
            RegimeTrabalho.toEnum(servidorDTO.regimeTrabalho.toString()),
            TipoDeVinculo.toEnum(servidorDTO.tipoDeVinculo.toString()),
            SituacaoServidor.toEnum(servidorDTO.situacaoServidor.toString()));
    }

    public fromDTOList(servidorDTOList: Servidor[]): Servidor[] {
        const servidorList = new Array<Servidor>();
        servidorDTOList.forEach((servidorDTO) => {
            servidorList.push(this.fromDTO(servidorDTO));
        });
        return servidorList;
    }
}
