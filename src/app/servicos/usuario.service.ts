import {Servidor} from '../models/servidor.model';
import {API_CONFIG} from '../config/api.config';
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Route, Router} from '@angular/router';
import {LocalUser} from '../security/login/local_user';
import {StorageService} from '../security/storage.service';
import {CredenciaisDto} from '../models/credenciais.dto';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Observable} from 'rxjs';
import {MensagemRetorno} from '../models/mensagem-retorno.model';
import {Usuario} from '../models/usuario.model';
import {PerfilUsuario, PerfilUsuarioModel} from '../models/enums/perfil-usuario.enum';

@Injectable()
export class UsuarioService {

    lastUrl: string;
    private jwtHelper = new JwtHelperService();
    private servidor: Servidor;

    constructor(
        private http: HttpClient,
        public storage: StorageService,
        private router: Router) {
    }

    public getLocalUser(): LocalUser {
        return this.storage.getLocalUser();
    }

    public setServidor(servidor: Servidor): void {
        this.servidor = this.fromDTO(servidor);
    }

    public getServidorAtual(): Promise<Servidor> {
        return new Promise(resolve => {
            if (this.servidor) {
                resolve(this.servidor);
            } else {
                this.getServidorPorLogin(this.getLocalUser().email)
                    .subscribe(servidor => {
                        this.servidor = this.fromDTO(servidor);
                        resolve(this.servidor);
                    });
            }
        });
    }

    public getServidorPorLogin(login: string): Observable<Servidor> {
        return this.http.get<Servidor>(`${API_CONFIG.baseUrl}/servidores/buscarUsuario/${login}`);
    }

    public getUsuarioPorLogin(login: string): Observable<Usuario> {
        return this.http.get<Usuario>(`${API_CONFIG.baseUrl}/usuarios/buscarUsuario/${login}`);
    }

    isLoggedIn(): boolean {
        const localUser = this.storage.getLocalUser();

        if (!localUser) {
            return false;
        }
        return !this.jwtHelper.isTokenExpired(localUser.token);
    }

    authenticate(creds: CredenciaisDto): Observable<HttpResponse<Object>> {
        return this.http.post<HttpResponse<any>>(`${API_CONFIG.baseUrl}/login`,
            creds, {observe: 'response'});
    }

    refreshToken() {
        return this.http.post(`${API_CONFIG.baseUrl}/auth/refresh_token`,
            {},
            {observe: 'response', responseType: 'text'});
    }

    successFullLogin(authorizationValue: string) {
        const tok = authorizationValue.substring(7);
        const user: LocalUser = {
            token: tok,
            email: this.jwtHelper.decodeToken(tok).sub
        };
        this.storage.setLocalUser(user);
    }

    logout() {
        this.storage.setLocalUser(null);
        this.router.navigate(['/login']);
    }

    handleLogin(path: string = this.lastUrl) {
        this.router.navigate(['/login', btoa(path)]);
    }

    hasPermission(route: Route): Observable<boolean> {
        return new Observable(observer => {
            if (!this.isLoggedIn()) {
                observer.next(false);
                this.router.navigate(['/login']);
                observer.complete();
                return;
            }

            this.getServidorAtual().then((servidor: Servidor) => {
                if (servidor.contemPermissao('' + route.data[0])) {
                    observer.next(true);
                } else {
                    observer.next(false);
                    this.router.navigate(['/naoAutorizado']);
                }
                observer.complete();
            }).catch((erro) => {
                console.log(erro);
                observer.next(false);
                observer.complete();
            });
        });
    }

    public inserir(usuario: Usuario): Observable<MensagemRetorno> {
        return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/usuarios/inserir`, usuario);
    }

    public editar(usuario: Usuario): Observable<MensagemRetorno> {
        const url = `${API_CONFIG.baseUrl}/usuarios`;
        return this.http.put<MensagemRetorno>(url, usuario);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
        return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/usuarios/${id}`);
    }

    public fromDTO(servidorDTO: Servidor): Servidor {
        const servidor = new Servidor(
            servidorDTO.idServidor,
            servidorDTO.nome,
            servidorDTO.email,
            servidorDTO.matriculaSiape,
            servidorDTO.fone,
            servidorDTO.departamentoArea,
            servidorDTO.regimeTrabalho,
            servidorDTO.tipoDeVinculo,
            servidorDTO.situacaoServidor
        );
        servidor.usuario = servidorDTO.usuario;
        return servidor;
    }

    public async redirecionarParaInicio() {
        const servidor = await this.getServidorAtual();
        let navegarPara = '/';
        if (servidor) {
            if (servidor.contemPermissao(PerfilUsuarioModel.GESTOR)) {
                navegarPara = '/gestao-pit';
            }
            if (servidor.contemPermissao(PerfilUsuarioModel.SERVIDOR)) {
                navegarPara = '/';
            }
            if (servidor.contemPermissao(PerfilUsuarioModel.ADMIN)) {
                navegarPara = '/gestao';
            }
        }
        this.router.navigate([navegarPara]);
    }
}
