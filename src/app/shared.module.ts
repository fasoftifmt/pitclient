import {NgModule} from '@angular/core';
import {MessageService, TooltipModule} from 'primeng/primeng';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {ToastModule} from 'primeng/toast';
import {PlanoDeTrabalhoService} from './servicos/plano-de-trabalho.service';
import {InputComponent} from './view/shared/input/input.component';
import {LoginComponent} from './security/login/login.component';
import {AuthInterceptor} from './security/auth.interceptor';
import {CommonModule} from '@angular/common';
import {LoggedInGuard} from './security/loggedin.guard';
import {StorageService} from './security/storage.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {PerfilGuard} from './security/perfil.guard';
import {NaoAutorizadoComponent} from './security/nao-autorizado/nao-autorizado.component';
import {RelatoriosService} from './servicos/relatorios.service';
import {GrupoAtividadeComponent} from './view/pit-view/grupo-atividade-view/grupo-atividade.component';

@NgModule({

    declarations: [
        InputComponent,
        LoginComponent,
        GrupoAtividadeComponent,
        NaoAutorizadoComponent
    ],
    imports: [
        TooltipModule,
        HttpClientModule,
        ReactiveFormsModule,
        ToastModule,
        CommonModule,
    ],
    providers: [
        MessageService,
        PlanoDeTrabalhoService,
        LoggedInGuard,
        PerfilGuard,
        JwtHelperService,
        RelatoriosService,
        StorageService,
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
    ],
    exports: [
        InputComponent,
        LoginComponent,
        NaoAutorizadoComponent,
        GrupoAtividadeComponent,
        TooltipModule,
        HttpClientModule,
        ReactiveFormsModule,
        ToastModule,
    ]
})
export class SharedModule {
}
