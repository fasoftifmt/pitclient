export class NumberUtil {

    public static arredondar(numero: number): number {
        return Number(numero.toFixed(2));
    }
}
