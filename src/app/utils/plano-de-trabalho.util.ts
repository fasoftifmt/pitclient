import {StatusPtd} from '../models/enums/status-ptd.enum';

export class PitUtil {

    public static statusPITToString(status: StatusPtd): string {
        switch (status) {
            case StatusPtd.ENVIADO:
                return 'Enviado';
            case  StatusPtd.APROVADO:
                return 'Aprovado';
            case StatusPtd.PLANEJADO:
                return 'Elaborado';
            case StatusPtd.PUBLICADO:
                return 'Publicado';
            case StatusPtd.ARQUIVADO:
                return 'Arquivado';
            default:
                return '';
        }
    }
}
