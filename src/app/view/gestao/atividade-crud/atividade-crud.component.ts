import {Atividade} from './../../../models/atividade.model';
import {AtividadeDTO} from './../../../models/dtos/atividade.dto';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'primeng/api';
import {AtividadeService} from 'src/app/servicos/atividade.service';
import {DirecaoOrder} from 'src/app/models/enums/direcao-order.enum';
import {MensagemRetorno} from 'src/app/models/mensagem-retorno.model';
import {UnidadeAtividade} from 'src/app/models/enums/unidade-atividade.enum';
import {CategoriaAtividade} from 'src/app/models/categoria-atividade.model';
import {CategoriaAtividadeServico} from 'src/app/servicos/categoria-atividade.service';


@Component({
    selector: 'pit-atividade-crud',
    templateUrl: './atividade-crud.component.html',
    styleUrls: ['./atividade-crud.component.scss']
})
export class AtividadeCrudComponent implements OnInit {

    atividadeForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    atividades: Array<Atividade>;
    atividadesNaoSub: Array<Atividade>;
    subAtividades: Array<Atividade>;
    atividadesComSubAtividades: Array<Atividade>;
    subAtividadesParaRemocao: Array<Atividade>;
    isEmEdicao = false;
    atividadeEmEdicao: Atividade;
    unidadeAtividadeList = UnidadeAtividade.getValues();
    categorias: Array<CategoriaAtividade>;
    ids: Array<number>;


    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private atividadeService: AtividadeService,
                private categoriaService: CategoriaAtividadeServico) {
    }

    ngOnInit() {
        this.ids = [];
        this.subAtividades = [];
        this.atividadesNaoSub = [];
        this.subAtividadesParaRemocao = [];
        document.getElementById('titulo-gestao').innerText = 'Atividade';
        this.buscarDados();
        this.buscarCategorias();
        this.buscarApenasAtividades();
        this.buscarApenasSubAtividades();
        this.buscarAtividadesComSubAtividades();

        this.atividadeForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            dicapreenchimento: this.fb.control('', [Validators.required]),
            fator: this.fb.control('', [Validators.required]),
            permitelancamentos: this.fb.control('', [Validators.required]),
            qtdemaxima: this.fb.control('', [Validators.required]),
            unidadeAtividade: this.fb.control('', [Validators.required]),
            categoria: this.fb.control('', [Validators.required]),
            isSubAtividade: this.fb.control('', [Validators.required]),
            atividadeVinculo: this.fb.control('', [Validators.required]),
            subAtividadeVinculo: this.fb.control('', [Validators.required]),
            atividadeDesfazerVinculo: this.fb.control('', [Validators.required]),
            subAtividadeDesfazerVinculo: this.fb.control('', [Validators.required]),
        });
    }

    salvar() {
        const descricao = this.atividadeForm.value.descricao;
        const dicapreenchimento = this.atividadeForm.value.dicapreenchimento;
        const fator = this.atividadeForm.value.fator;
        const permitelancamentos = this.atividadeForm.value.permitelancamentos;
        const qtdemaxima = this.atividadeForm.value.qtdemaxima;
        const unidadeatividade = this.atividadeForm.value.unidadeAtividade;
        const isSubAtividade = this.atividadeForm.value.isSubAtividade;
        const categoria = this.atividadeForm.value.categoria;

        let isDadosValidos = true;

        let msg = '';

        if (!descricao || descricao === '') {
            this.atividadeForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!dicapreenchimento || dicapreenchimento === '') {
            this.atividadeForm.controls['dicapreenchimento'].setErrors({'incorrect': true});
            msg = 'O campo dica de preenchimento é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!fator || fator === '') {
            this.atividadeForm.controls['fator'].setErrors({'incorrect': true});
            msg = 'O campo fator de preenchimento é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }


        if (permitelancamentos === '') {
            this.atividadeForm.controls['permitelancamentos'].setErrors({'incorrect': true});
            msg = 'O campo permite lançamentos é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!qtdemaxima || qtdemaxima === '') {
            this.atividadeForm.controls['qtdemaxima'].setErrors({'incorrect': true});
            msg = 'O campo quantidade máxima é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!unidadeatividade || unidadeatividade === '') {
            this.atividadeForm.controls['unidadeAtividade'].setErrors({'incorrect': true});
            msg = 'O campo unidade atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (isSubAtividade === '') {
            this.atividadeForm.controls['isSubAtividade'].setErrors({'incorrect': true});
            msg = 'O campo Sub-atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!categoria || categoria === '') {
            this.atividadeForm.controls['categoria'].setErrors({'incorrect': true});
            msg = 'O campo categoria é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const atividade = new AtividadeDTO(
            descricao,
            qtdemaxima,
            dicapreenchimento,
            fator,
            unidadeatividade,
            10,
            permitelancamentos,
            isSubAtividade,
            categoria);

        // atividade.isSubAtividade = isSubAtividade;

        console.log(atividade);
        if (this.isEmEdicao) {
            atividade.id = this.atividadeEmEdicao.id;
            this.atualizar(atividade);
        } else {
            this.inserir(atividade);
        }

    }

    public inserir(atividade: AtividadeDTO): void {
        this.atividadeService.inserir(atividade)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
                this.atividadeForm.reset();
                this.buscarDados();
                this.buscarApenasAtividades();
                this.buscarApenasSubAtividades();
            }, error => {
                this.exibirMsgErro('Erro ao salvar registro');
            });
    }


    public editar(atividade: Atividade) {
        this.atividadeForm.controls['descricao'].setValue(atividade.descricao);
        this.atividadeForm.controls['fator'].setValue(atividade.fator);
        this.atividadeForm.controls['dicapreenchimento'].setValue(atividade.dicaPreenchimento);
        this.atividadeForm.controls['qtdemaxima'].setValue(atividade.qtdMax);
        this.atividadeForm.controls['permitelancamentos'].setValue(atividade.permiteLancamentoMultiplos ? 1 : 0);
        this.atividadeForm.controls['unidadeAtividade'].setValue(atividade.unidadeAtividade);
        this.atividadeForm.controls['categoria'].setValue(atividade.categoria.id);
        this.atividadeForm.controls['isSubAtividade'].setValue(atividade.isSubAtividade ? 1 : 0);

        console.log(atividade);
        console.log(atividade.isSubAtividade);

        this.isEmEdicao = true;
        this.atividadeEmEdicao = atividade;
    }

    public excluir(atividade: Atividade) {
        this.atividadeService.excluir(atividade.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.atividades.splice(this.atividades.indexOf(atividade, 0), 1);
                this.qtdLinhas--;
                this.buscarApenasAtividades();
                this.buscarApenasSubAtividades();
                this.buscarAtividadesComSubAtividades();

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    vincularAtividades() {

        let isDadosVinculoValidos = true;
        let msg = '';

        const atividadeID = this.atividadeForm.value.atividadeVinculo;
        const subAtividadeID = this.atividadeForm.value.subAtividadeVinculo;

        if (!atividadeID || atividadeID === '') {
            this.atividadeForm.controls['atividadeVinculo'].setErrors({'incorrect': true});
            msg = 'O campo Atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosVinculoValidos = false;
        }

        if (!subAtividadeID || subAtividadeID === '') {
            this.atividadeForm.controls['subAtividadeVinculo'].setErrors({'incorrect': true});
            msg = 'O campo isSubAtividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosVinculoValidos = false;
        }

        if (!isDadosVinculoValidos) {
            return;
        }
        this.ids[0] = atividadeID;
        this.ids[1] = subAtividadeID;

        this.realizaVinculo(this.ids);

    }

    realizaVinculo(ids: Array<number>) {


        this.atividadeService.vincularAtividades(this.ids)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Vinculo realizado com sucesso'
                });
                this.limparCamposVinculo();
                this.buscarDados();
                this.buscarApenasAtividades();
                this.buscarApenasSubAtividades();
                this.buscarAtividadesComSubAtividades();
            }, error => {
                console.log(error);
                this.exibirMsgErro('Erro ao realizar vinculo');
            });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {

        this.atividadeService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.atividades = res.content;
                this.qtdLinhas = res.totalElements;
            });
    }

    buscarCategorias() {

        this.categoriaService.findAll()
            .subscribe((categorias: CategoriaAtividade[]) => {
                this.categorias = categorias;
            });
    }

    buscarApenasAtividades() {

        this.atividadesNaoSub = [];
        this.atividadeService.findAll()
            .subscribe((at: Atividade[]) => {
                for (var x = 0; x < at.length; x++) {
                    if (!at[x].isSubAtividade) {
                        this.atividadesNaoSub.push(at[x]);
                    }
                }
            });
    }

    buscarApenasSubAtividades() {
        this.subAtividades = [];
        this.atividadeService.findAll()
            .subscribe((at: Atividade[]) => {
                for (var x = 0; x < at.length; x++) {
                    if (at[x].isSubAtividade) {
                        this.subAtividades.push(at[x]);
                    }
                }
            });
    }

    buscarAtividadesComSubAtividades() {

        this.atividadesComSubAtividades = [];
        this.atividadeService.findAll()
            .subscribe((at: Atividade[]) => {
                for (var x = 0; x < at.length; x++) {
                    if (!at[x].isSubAtividade && at[x].subAtividadeList.length > 0) {
                        this.atividadesComSubAtividades.push(at[x]);
                    }
                }
            });
    }

    buscarSubRelacionadas() {

        this.atividadeForm.controls['subAtividadeDesfazerVinculo'].reset();
        this.subAtividadesParaRemocao = [];

        const atividadeID = this.atividadeForm.value.atividadeDesfazerVinculo;


        console.log(atividadeID);
        var atTemp: Atividade;
        for (var x = 0; x < this.atividadesComSubAtividades.length; x++) {
            if (this.atividadesComSubAtividades[x].id == atividadeID) {
                atTemp = this.atividadesComSubAtividades[x];
            }
        }

        this.subAtividadesParaRemocao = atTemp.subAtividadeList;

    }

    desvincularAtividades() {
        const atividadeID = this.atividadeForm.value.atividadeDesfazerVinculo;
        const subAtividadeID = this.atividadeForm.value.subAtividadeDesfazerVinculo;

        let isDadosDesvinculoValidos = true;
        let msg = '';


        if (!atividadeID || atividadeID === '') {
            this.atividadeForm.controls['atividadeDesfazerVinculo'].setErrors({'incorrect': true});
            msg = 'O campo Atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosDesvinculoValidos = false;
        }

        if (!subAtividadeID || subAtividadeID === '') {
            this.atividadeForm.controls['subAtividadeDesfazerVinculo'].setErrors({'incorrect': true});
            msg = 'O campo isSubAtividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosDesvinculoValidos = false;
        }

        if (!isDadosDesvinculoValidos) {
            return;
        }
        this.ids[0] = atividadeID;
        this.ids[1] = subAtividadeID;

        this.realizaDesvinculo(this.ids);


    }

    realizaDesvinculo(ids: Array<number>) {

        this.atividadeService.desvincularAtividades(this.ids)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Desvinculo realizado com sucesso'
                });

                this.buscarDados();
                this.buscarApenasAtividades();
                this.buscarApenasSubAtividades();
                this.buscarAtividadesComSubAtividades();
                this.limparCamposDesvinculo();
            }, error => {
                console.log(error);
                this.exibirMsgErro('Erro ao realizar desvinculo');
            });


    }

    public exibirMsgErro(msg: string): void {
        console.log(msg);
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public resetarEdicao(): void {
        this.atividadeForm.controls['descricao'].reset();
        this.atividadeForm.controls['fator'].reset();
        this.atividadeForm.controls['dicapreenchimento'].reset();
        this.atividadeForm.controls['qtdemaxima'].reset();
        this.atividadeForm.controls['permitelancamentos'].reset();
        this.atividadeForm.controls['unidadeAtividade'].reset();
        this.atividadeForm.controls['categoria'].reset();
        this.atividadeForm.controls['isSubAtividade'].reset();
        this.isEmEdicao = false;
        this.atividadeEmEdicao = null;
    }

    public limparCamposVinculo(): void {
        this.atividadeForm.controls['atividadeVinculo'].reset();
        this.atividadeForm.controls['subAtividadeVinculo'].reset();
    }

    public limparCamposDesvinculo(): void {
        this.atividadeForm.controls['atividadeDesfazerVinculo'].reset();
        this.atividadeForm.controls['subAtividadeDesfazerVinculo'].reset();
    }

    private atualizar(atividade: AtividadeDTO): void {
        this.atividadeService.editar(atividade)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
                this.buscarApenasAtividades();
                this.buscarApenasSubAtividades();
                this.buscarAtividadesComSubAtividades();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }
}
