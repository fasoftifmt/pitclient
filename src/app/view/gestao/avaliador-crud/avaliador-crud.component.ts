import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DirecaoOrder} from '../../../models/enums/direcao-order.enum';
import {Avaliador} from '../../../models/avaliador.model';
import {Message, MessageService} from 'primeng/api';
import {AvaliadorServico} from '../../../servicos/avaliador.service';
import {Campus} from '../../../models/campus.model';
import {Servidor} from '../../../models/servidor.model';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';
import {CampusService} from '../../../servicos/campus.service';
import {ServidorService} from '../../../servicos/servidor.service';


@Component({
    selector: 'pit-avaliador-crud',
    templateUrl: './avaliador-crud.component.html',
    styleUrls: ['./avaliador-crud.component.scss'],
    providers: [CampusService]
})
export class AvaliadorCrudComponent implements OnInit {

    avaliadorForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    indexLinha = 0;
    linhasPorPagina = 5;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    avaliador: Array<Avaliador>;

    isEmEdicao = false;
    avaliadorEmEdicao: Avaliador;
    campusList: Array<Campus>;
    servidorList: Array<Servidor>;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private avaliadorService: AvaliadorServico,
                private campusService: CampusService,
                private servidorService: ServidorService) { }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Avaliador';
        this.buscarDados();
        this.avaliadorForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            campus: this.fb.control(null, [Validators.required]),
            servidor: this.fb.control(null, [Validators.required])
        });
    }

    salvar() {
        const descricao = this.avaliadorForm.value.descricao;
        const servidor = this.avaliadorForm.value.servidor;
        const campus = this.avaliadorForm.value.campus;
        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.avaliadorForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!campus || campus === '') {
           this.avaliadorForm.controls['campus'].setErrors({'incorrect': true});
           msg = 'O campo Campus é obrigatório';
           this.exibirMsgErro(msg);
           isDadosValidos = false;
        }
        if (!servidor || servidor === '') {
            this.avaliadorForm.controls['servidor'].setErrors({'incorrect': true});
            msg = 'O campo Servidor é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!isDadosValidos) {
            return;
        }

        const avaliador = new Avaliador(descricao, servidor, campus);
        if (this.isEmEdicao) {
            avaliador.idAvaliador = this.avaliadorEmEdicao.idAvaliador;
            this.atualizar(avaliador);
        } else {
            this.inserir(avaliador);
        }
    }

    public inserir(avaliador: Avaliador): void {
        this.avaliadorService.inserir(avaliador).subscribe((msgRetorno: MensagemRetorno) => {
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro salvo com sucesso'
            });
            this.avaliadorForm.reset();
            this.buscarDados();
        }, error => {
            this.exibirMsgErro('Erro ao salvar o Registro');
        });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }
    // public buscarServidores() {
    //     this.servidorService
    //         .listarTodos()
    //         .subscribe( servidorList => {
    //             this.servidorList = servidorList;
    //         });
    // }

    public buscarDados() {
        this.avaliadorService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao).subscribe((res: any) => {
            this.avaliador = res.content;
            this.qtdLinhas = res.totalElements;
        });
        this.campusService.listarTodos().subscribe(campusList => {
            this.campusList = campusList;
        });
        this.servidorService.listarTodos().subscribe( servidorList => {
           this.servidorList = servidorList;
        });
    }

    public editar(ava: Avaliador) {
        this.avaliadorForm.controls['descricao'].setValue(ava.descricao);
        this.avaliadorForm.controls['servidor'].setValue(
            this.servidorList.find(value => value.idServidor === ava.servidor.idServidor));
        this.avaliadorForm.controls['campus'].setValue(
            this.campusList.find(value => value.id === ava.campus.id));
        this.isEmEdicao = true;
        this.avaliadorEmEdicao = ava;
    }

    public excluir(ava: Avaliador) {
        this.avaliadorService.excluir(ava.idAvaliador)
            .subscribe((msg: MensagemRetorno) => {
            if (!msg.sucesso) {
                this.exibirMsgErro(msg.msgList.toString());
                return;
            }
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro Excluido com sucesso'
            });

            this.avaliador.splice(
                this.avaliador.indexOf(ava, 0), 1);
            this.qtdLinhas--;
        }, error1 => {
            this.exibirMsgErro('Erro ao tentar excluir registro');
        });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public resetarEdicao(): void {
        this.avaliadorForm.reset();
        this.isEmEdicao = false;
        this.avaliadorEmEdicao = null;
    }

    private atualizar(avaliador: Avaliador): void {
        this.avaliadorService
            .editar(avaliador)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });
                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }
}
