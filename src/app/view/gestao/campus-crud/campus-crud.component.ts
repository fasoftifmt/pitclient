import {Component, OnInit} from '@angular/core';
import {CampusService} from 'src/app/servicos/campus.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DirecaoOrder} from 'src/app/models/enums/direcao-order.enum';
import {MensagemRetorno} from 'src/app/models/mensagem-retorno.model';
import {Campus} from 'src/app/models/campus.model';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'pit-campus-crud',
    templateUrl: './campus-crud.component.html',
    styleUrls: ['./campus-crud.component.scss'],
    providers: [CampusService]
})

export class CampusCrudComponent implements OnInit {

    campusForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'nome';
    direcao = DirecaoOrder.ASC;
    campus: Array<Campus>;

    isEmEdicao = false;
    campusEmEdicao: Campus;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private campusService: CampusService) {
    }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Campus';
        this.buscarDados();
        this.campusForm = this.fb.group({
            nome: this.fb.control('', [Validators.required])
        });
    }

    salvar() {
        const id = this.campusForm.value.id;
        const nome = this.campusForm.value.nome;
        let isDadosValidos = true;
        let msg = '';

        if (!nome || nome === '') {
            this.campusForm.controls['nome'].setErrors({'incorret': true});
            msg = 'O Campo nome é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const campus = new Campus(nome);
        if (this.isEmEdicao) {
            campus.id = this.campusEmEdicao.id;
            this.atualizar(campus);
        } else {
            this.inserir(campus);
        }
    }

    public inserir(campus: Campus): void {
        this.campusService.inserir(campus).subscribe((msgRetorno: MensagemRetorno) => {
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro salvo com sucesso'
            });
            this.campusForm.reset();
            this.buscarDados();
        }, error => {
            this.exibirMsgErro('Erro ao salvar registro');
        });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {

        this.campusService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.campus = res.content;
                this.qtdLinhas = res.totalElements;
            });
    }

    public editar(cmp: Campus) {
        this.campusForm.controls['nome'].setValue(cmp.nome);
        this.isEmEdicao = true;
        this.campusEmEdicao = cmp;
    }

    public excluir(cmp: Campus) {
        this.campusService.excluir(cmp.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.campus.splice(
                    this.campus.indexOf(cmp, 0), 1);
                this.qtdLinhas--;

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public exibirMsg(msg: MensagemRetorno): void {
        if (msg) {
            msg.msgList.forEach(element => {
                this.exibirMsgErro(element);
            });
        }
    }

    public resetarEdicao(): void {
        this.campusForm.reset();
        this.isEmEdicao = false;
        this.campusEmEdicao = null;
    }

    private atualizar(campus: Campus): void {
        this.campusService.editar(campus)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }

}
