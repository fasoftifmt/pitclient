import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'primeng/api';
import {CategoriaAtividadeServico} from '../../../servicos/categoria-atividade.service';
import {DirecaoOrder} from '../../../models/enums/direcao-order.enum';
import {CategoriaAtividade} from '../../../models/categoria-atividade.model';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';
import {GrupoAtividade} from '../../../models/grupo-atividade.model';
import {GrupoAtividadeService} from '../../../servicos/grupo-atividade.service';

@Component({
    selector: 'pit-categoria-atividade-crud',
    templateUrl: './categoria-atividade-crud.component.html',
    styleUrls: ['./categoria-atividade-crud.component.scss'],
    providers: [GrupoAtividadeService]
})
export class CategoriaAtividadeCrudComponent implements OnInit {

    categoriaAtividadeForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    categoriaAtividade: Array<CategoriaAtividade>;

    grupoList: Array<GrupoAtividade>

    isEmEdicao = false;
    categoriaAtividadeEmEdicao: CategoriaAtividade;

    constructor(private fb: FormBuilder,
                private grupoAtividadeService: GrupoAtividadeService,
                private messageService: MessageService,
                private categoriaAtividadeService: CategoriaAtividadeServico) {
    }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Categoria Atividade';
        this.buscarDados();
        this.categoriaAtividadeForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            ordem: this.fb.control('', [Validators.required]),
            grupoAtividade: this.fb.control(null, [Validators.required])
        });
    }

    salvar() {
        const descricao = this.categoriaAtividadeForm.value.descricao;
        const ordem = this.categoriaAtividadeForm.value.ordem;
        const grupoAtividade = this.categoriaAtividadeForm.value.grupoAtividade;

        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.categoriaAtividadeForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!ordem || ordem === '') {
            this.categoriaAtividadeForm.controls['ordem'].setErrors({'incorrect': true});
            msg = 'O campo ordem é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if( !grupoAtividade || grupoAtividade === ''){
            this.categoriaAtividadeForm.controls['grupoAtividade'].setErrors({'incorrect': true});
            msg = 'O campo grupo atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }
        const categoriaAtividade = new CategoriaAtividade(descricao, ordem, grupoAtividade);
        if (this.isEmEdicao) {
            categoriaAtividade.id = this.categoriaAtividadeEmEdicao.id;
            this.atualizar(categoriaAtividade);
        } else {
            this.inserir(categoriaAtividade);
        }
    }

    public inserir(categoriaAtividade: CategoriaAtividade): void {
        this.categoriaAtividadeService
            .inserir(categoriaAtividade)
            .subscribe((msgRetorno: MensagemRetorno) => {
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro salvo com sucesso'
            });
            this.categoriaAtividadeForm.reset();
            this.buscarDados();
        }, error => {
            this.exibirMsgErro('erro ao salvar registro');
        });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {
        this.categoriaAtividadeService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao).subscribe((res: any) => {
            this.categoriaAtividade = res.content;
            this.qtdLinhas = res.totalElements;
        });
        this.grupoAtividadeService.listarTodos()
            .subscribe( grupoList => {
               this.grupoList = grupoList;
            });
    }

    public editar(catAt: CategoriaAtividade) {
        this.categoriaAtividadeForm.controls['descricao'].setValue(catAt.descricao);
        this.categoriaAtividadeForm.controls['ordem'].setValue(catAt.ordem);
        this.categoriaAtividadeForm.controls['grupoAtividade'].setValue(
            this.grupoList.find( value => value.id === catAt.grupoAtividade.id)
        );
        this.isEmEdicao = true;
        this.categoriaAtividadeEmEdicao = catAt;
    }

    public excluir(catAt: CategoriaAtividade) {
        this.categoriaAtividadeService
            .excluir(catAt.id)
            .subscribe((msg: MensagemRetorno) => {
                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Regsitro excluido com sucesso'
            });

            this.categoriaAtividade.splice(
                this.categoriaAtividade.indexOf(catAt, 0), 1);
            this.qtdLinhas--;
        }, erro1 => {
            this.exibirMsgErro('Erro ao Tentar excluir registro');
        });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public atualizar(categoriaAtividade: CategoriaAtividade): void {
        this.categoriaAtividadeService
            .editar(categoriaAtividade)
            .subscribe((msgRetorno: MensagemRetorno) => {
            if (!msgRetorno.sucesso) {
                this.exibirMsgErro(msgRetorno.msgList.toString());
                return;
            }
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro editado com sucesso'
            });
            this.buscarDados();
            this.resetarEdicao();
        }, error => {
            this.exibirMsgErro('Erro ao tentar editar o regsistro');
        });
    }

    public resetarEdicao(): void {
        this.categoriaAtividadeForm.reset();
        this.isEmEdicao = false;
        this.categoriaAtividadeEmEdicao = null;
    }
}
