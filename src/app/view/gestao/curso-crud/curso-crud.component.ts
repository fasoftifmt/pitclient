import {CursoDTO} from '../../../models/dtos/curso.dto';
import {Departamento} from '../../../models/departamento.model';
import {DepartamentoServico} from 'src/app/servicos/departamento.service';
import {Curso} from '../../../models/curso.model';
import {CursoService} from '../../../servicos/curso.service';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DirecaoOrder} from 'src/app/models/enums/direcao-order.enum';
import {MessageService} from 'primeng/api';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';

@Component({
    selector: 'pit-curso-crud',
    templateUrl: './curso-crud.component.html',
    styleUrls: ['./curso-crud.component.scss']
})
export class CursoCrudComponent implements OnInit {


    cursoForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    cursos: Array<Curso>;
    departamentos: Array<Departamento>;

    isEmEdicao = false;
    cursoEmEdicao: Curso;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private cursoService: CursoService,
                private departamentoServico: DepartamentoServico) {
    }

    ngOnInit() {

        document.getElementById('titulo-gestao').innerText = 'Curso';
        this.buscarDados();
        this.buscarDepartamentos();
        this.cursoForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            departamento: this.fb.control('', [Validators.required])
        });

    }

    salvar() {
        const descricao = this.cursoForm.value.descricao;
        const departamentoId = this.cursoForm.value.departamento;
        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.cursoForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O Campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!departamentoId || departamentoId === '') {
            this.cursoForm.controls['departamento'].setErrors({'incorrect': true});
            msg = ' O campo departamento é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const curso = new CursoDTO(descricao);
        curso.departamentoID = departamentoId;
        console.log(curso);
        if (this.isEmEdicao) {
            curso.id = this.cursoEmEdicao.id;
            this.atualizar(curso);
        } else {
            this.inserir(curso);
        }
    }

    public inserir(curso: CursoDTO): void {
        this.cursoService.inserir(curso)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
                this.cursoForm.reset();
                this.buscarDados();
            }, error => {
                this.exibirMsgErro('Erro ao salvar registro');
            });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {

        this.cursoService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.cursos = res.content;
                this.qtdLinhas = res.totalElements;
            });
    }

    buscarDepartamentos() {

        this.departamentoServico.findAll()
            .subscribe((dep: Departamento[]) => {
                this.departamentos = dep;
            });
    }

    public editar(curso: Curso) {
        this.cursoForm.controls['descricao'].setValue(curso.descricao);
        this.cursoForm.controls['departamento'].setValue(curso.departamentoArea.id);
        this.isEmEdicao = true;
        this.cursoEmEdicao = curso;
    }

    public excluir(curso: Curso) {
        this.cursoService.excluir(curso.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.cursos.splice(
                    this.cursos.indexOf(curso, 0), 1);
                this.qtdLinhas--;

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public resetarEdicao(): void {
        this.cursoForm.reset();
        this.isEmEdicao = false;
        this.cursoEmEdicao = null;
    }

    private atualizar(curso: CursoDTO): void {
        this.cursoService.editar(curso)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }


}


