import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'primeng/api';
import {Departamento} from 'src/app/models/departamento.model';
import {DepartamentoServico} from 'src/app/servicos/departamento.service';
import {DirecaoOrder} from '../../../models/enums/direcao-order.enum';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';
import {Campus} from '../../../models/campus.model';
import {CampusService} from '../../../servicos/campus.service';

@Component({
    selector: 'pit-departamento-crud',
    templateUrl: './departamento-crud.component.html',
    styleUrls: ['./departamento-crud.component.scss'],
    providers: [CampusService]
})
export class DepartamentoCrudComponent implements OnInit {

    departamentoForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    departamentos: Array<Departamento>;

    isEmEdicao = false;
    departamentoEmEdicao: Departamento;
    campusList: Array<Campus>;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private campusService: CampusService,
                private departamentoService: DepartamentoServico) {
    }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Departamento/Area';
        this.buscarDados();
        this.departamentoForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            sigla: this.fb.control('', [Validators.required]),
            campus: this.fb.control(null, [Validators.required])
        });
    }

    salvar() {
        const descricao = this.departamentoForm.value.descricao;
        const sigla = this.departamentoForm.value.sigla;
        const campus = this.departamentoForm.value.campus;
        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.departamentoForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O Campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!sigla || sigla === '') {
            this.departamentoForm.controls['sigla'].setErrors({'incorrect': true});
            msg = 'O Campo sigla é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!campus || campus === '') {
            this.departamentoForm.controls['campus'].setErrors({'incorrect': true});
            msg = 'O Campo Campus é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const departamento = new Departamento(descricao, sigla, campus);
        if (this.isEmEdicao) {
            departamento.id = this.departamentoEmEdicao.id;
            this.atualizar(departamento);
        } else {
            this.inserir(departamento);
        }

    }

    public inserir(departamento: Departamento): void {
        this.departamentoService.inserir(departamento)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
                this.departamentoForm.reset();
                this.buscarDados();
            }, error => {
                this.exibirMsgErro('Erro ao salvar registro');
            });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {

        this.departamentoService
            .buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.departamentos = res.content;
                this.qtdLinhas = res.totalElements;
            });

        this.campusService.listarTodos()
            .subscribe(campusList => {
                this.campusList = campusList;
            });
    }

    public editar(dep: Departamento) {
        this.departamentoForm.controls['descricao'].setValue(dep.descricao);
        this.departamentoForm.controls['sigla'].setValue(dep.sigla);
        this.departamentoForm.controls['campus'].setValue(
            this.campusList.find(value => value.id === dep.campus.id)
        );
        this.isEmEdicao = true;
        this.departamentoEmEdicao = dep;
    }

    public excluir(dep: Departamento) {
        this.departamentoService.excluir(dep.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.departamentos.splice(
                    this.departamentos.indexOf(dep, 0), 1);
                this.qtdLinhas--;

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public resetarEdicao(): void {
        this.departamentoForm.reset();
        this.isEmEdicao = false;
        this.departamentoEmEdicao = null;
    }

    private atualizar(departamento: Departamento): void {
        this.departamentoService
            .editar(departamento)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }
}
