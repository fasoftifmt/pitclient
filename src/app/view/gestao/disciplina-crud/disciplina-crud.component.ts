import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DirecaoOrder} from 'src/app/models/enums/direcao-order.enum';
import {Disciplina} from 'src/app/models/disciplina.model';
import {DisciplinaService} from 'src/app/view/gestao/relatorios/disciplina.service';
import {MessageService} from 'primeng/api';
import {MensagemRetorno} from 'src/app/models/mensagem-retorno.model';
import {CursoService} from 'src/app/servicos/curso.service';
import {Curso} from 'src/app/models/curso.model';
import {DisciplinaDTO} from 'src/app/models/dtos/disciplina.dto';

@Component({
    selector: 'pit-disciplina-crud',
    templateUrl: './disciplina-crud.component.html',
    styleUrls: ['./disciplina-crud.component.scss'],
    providers: [DisciplinaService]
})

export class DisciplinaCrudComponent implements OnInit {

    disciplinaForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    disciplinas: Array<Disciplina>;
    cursos: Curso[];

    isEmEdicao = false;
    disciplinaEmEdicacao: Disciplina;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private disciplinaService: DisciplinaService,
                private cursoService: CursoService) {

    }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Disciplina';
        this.buscarDados();
        this.disciplinaForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            cargaHoraria: this.fb.control('', [Validators.required]),
            numAulaSemanal: this.fb.control('', [Validators.required]),
            periodo: this.fb.control('', [Validators.required]),
            curso: this.fb.control('', [Validators.required])

        });
        this.listarCursos();

    }

    salvar() {
        const id = this.disciplinaForm.value.id;
        const descricao = this.disciplinaForm.value.descricao;
        const cargaHoraria = this.disciplinaForm.value.cargaHoraria;
        const numAulaSemanal = this.disciplinaForm.value.numAulaSemanal;
        const periodo = this.disciplinaForm.value.periodo;
        const curso = this.disciplinaForm.value.curso;
        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.disciplinaForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O Campo descrição é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!cargaHoraria || cargaHoraria === '') {
            this.disciplinaForm.controls['cargaHoraria'].setErrors({'incorrect': true});
            msg = 'O Campo carga horária é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!numAulaSemanal || numAulaSemanal === '') {
            this.disciplinaForm.controls['numAulaSemanal'].setErrors({'incorrect': true});
            msg = 'O Campo número de aulas semanais é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!periodo || periodo === '') {
            this.disciplinaForm.controls['periodo'].setErrors({'incorrect': true});
            msg = 'O Campo período é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!curso || curso === '') {
            this.disciplinaForm.controls['curso'].setErrors({'incorrect': true});
            msg = 'O Campo curso é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const disciplina = new DisciplinaDTO(id, descricao, cargaHoraria, numAulaSemanal, periodo, curso);
        if (this.isEmEdicao) {
            disciplina.id = this.disciplinaEmEdicacao.id;
            this.atualizar(disciplina);
        } else {
            this.inserir(disciplina);
        }
    }

    public inserir(disciplina: DisciplinaDTO): void {
        this.disciplinaService.inserir(disciplina)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
                this.disciplinaForm.reset();
                this.buscarDados();
            }, error => {
                this.exibirMsg(error.error);
            });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {
        this.disciplinaService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.disciplinas = res.content;
                this.qtdLinhas = res.totalElements;
            });
    }

    public editar(dis: Disciplina) {
        this.disciplinaForm.controls['descricao'].setValue(dis.descricao);
        this.disciplinaForm.controls['cargaHoraria'].setValue(dis.cargaHoraria);
        this.disciplinaForm.controls['numAulaSemanal'].setValue(dis.numAulaSemanal);
        this.disciplinaForm.controls['periodo'].setValue(dis.periodo);
        this.disciplinaForm.controls['curso'].setValue(dis.curso.id);
        this.isEmEdicao = true;
        this.disciplinaEmEdicacao = dis;
    }

    public excluir(dis: Disciplina) {
        this.disciplinaService.excluir(dis.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.disciplinas.splice(
                    this.disciplinas.indexOf(dis, 0), 1);
                this.qtdLinhas--;

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }


    public exibirMsg(msg: MensagemRetorno): void {
        if (msg) {
            msg.msgList.forEach(element => {
                this.exibirMsgErro(element);
            });
        }
    }

    public resetarEdicao(): void {
        this.disciplinaForm.reset();
        this.isEmEdicao = false;
        this.disciplinaEmEdicacao = null;
    }

    listarCursos(): void {
        this.cursoService.findAll().subscribe((cursos: Curso[]) => {
            this.cursos = cursos;
        }, erro => {
            this.messageService.add({
                severity: 'error',
                summary: 'Erro',
                detail: 'Erro ao buscar curso'
            });
        });
    }

    private atualizar(disciplina: DisciplinaDTO): void {
        this.disciplinaService.editar(disciplina)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }

}
