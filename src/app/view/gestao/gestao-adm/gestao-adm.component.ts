import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'pit-gestao-adm',
    templateUrl: './gestao-adm.component.html',
    styleUrls: ['./gestao-adm.component.scss'],
    animations: [
        trigger('rotatedState', [
            state('default', style({transform: 'rotate(0)'})),
            state('rotated', style({transform: 'rotate(-180deg)'})),
            transition('rotated => default', animate('500ms ease-out')),
            transition('default => rotated', animate('500ms ease-in'))
        ])
    ]
})
export class GestaoAdmComponent implements OnInit {

    classe = 'vz';
    state = 'default';

    constructor() {
    }

    ngOnInit() {
    }

    alternar() {
        this.classe = this.classe === 'vz' ? 'toggled' : 'vz';
        this.state = (this.state === 'default' ? 'rotated' : 'default');
    }
}
