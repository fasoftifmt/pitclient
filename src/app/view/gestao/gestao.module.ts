import {NgModule} from '@angular/core';
import {GrupoCategoriaService} from '../../servicos/grupo-categoria.service';
import {CategoriaService} from '../../servicos/categoria.service';
import {AtividadeService} from '../../servicos/atividade.service';
import {PaginatorModule} from 'primeng/paginator';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared.module';
import {CommonModule} from '@angular/common';
import {GestaoAdmComponent} from './gestao-adm/gestao-adm.component';
import {DepartamentoCrudComponent} from './departamento-crud/departamento-crud.component';
import {DepartamentoServico} from 'src/app/servicos/departamento.service';
import {DisciplinaCrudComponent} from './disciplina-crud/disciplina-crud.component';
import {CursoService} from 'src/app/servicos/curso.service';
import {ServidorCrudComponent} from './servidor-crud/servidor-crud.component';
import {CategoriaAtividadeCrudComponent} from './categoria-atividade-crud/categoria-atividade-crud.component';
import {CategoriaAtividadeServico} from '../../servicos/categoria-atividade.service';
import {CursoCrudComponent} from './curso-crud/curso-crud.component';
import {AtividadeCrudComponent} from './atividade-crud/atividade-crud.component';
import {ServidorService} from '../../servicos/servidor.service';
import {CampusCrudComponent} from './campus-crud/campus-crud.component';
import {GrupoAtividadeCRUDComponent} from './grupo-atividade-crud/grupo-atividade-crud.component';
import {GrupoAtividadeService} from 'src/app/servicos/grupo-atividade.service';
import {AvaliadorCrudComponent} from './avaliador-crud/avaliador-crud.component';
import {AvaliadorServico} from '../../servicos/avaliador.service';
import {DashbordComponent} from './dashbord/dashbord.component';


const ROUTES: Routes = [
    {
        path: '', component: GestaoAdmComponent,
        children: [
            {path: 'dashbord', component: DashbordComponent},
            {path: 'departamento', component: DepartamentoCrudComponent},
            {path: 'disciplina', component: DisciplinaCrudComponent},
            {path: 'usuario', component: ServidorCrudComponent},
            {path: 'curso', component: CursoCrudComponent},
            {path: 'servidor', component: ServidorCrudComponent},
            {path: 'categoria', component: CategoriaAtividadeCrudComponent},
            {path: 'atividade', component: AtividadeCrudComponent},
            {path: 'campus', component: CampusCrudComponent},
            {path: 'grupo-atividade', component: GrupoAtividadeCRUDComponent},
            {path: 'avaliador', component: AvaliadorCrudComponent},
            {path: '', redirectTo: 'dashbord', component: DashbordComponent}
            ]
    },
];

@NgModule({
    declarations: [
        GestaoAdmComponent,
        DepartamentoCrudComponent,
        DisciplinaCrudComponent,
        CursoCrudComponent,
        DashbordComponent,
        CampusCrudComponent,
        GrupoAtividadeCRUDComponent,
        CategoriaAtividadeCrudComponent,
        AtividadeCrudComponent,
        ServidorCrudComponent,
        AvaliadorCrudComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PaginatorModule,
        RouterModule.forChild(ROUTES)
    ],
    providers: [
        GrupoCategoriaService,
        DepartamentoServico,
        CategoriaService,
        AtividadeService,
        ServidorService,
        CursoService,
        GrupoAtividadeService,
        CategoriaAtividadeServico,
        AvaliadorServico
    ]
})
export class GestaoModule {
}
