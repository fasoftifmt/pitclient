import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DirecaoOrder} from 'src/app/models/enums/direcao-order.enum';
import {GrupoAtividade} from 'src/app/models/grupo-atividade.model';
import {MessageService} from 'primeng/api';
import {MensagemRetorno} from 'src/app/models/mensagem-retorno.model';
import {GrupoAtividadeService} from 'src/app/servicos/grupo-atividade.service';

@Component({
    selector: 'pit-grupo-atividade-crud',
    templateUrl: './grupo-atividade-crud.component.html',
    styleUrls: ['./grupo-atividade-crud.component.scss'],
    providers: [GrupoAtividadeService]
})

export class GrupoAtividadeCRUDComponent implements OnInit {

    grupoAtividadeForm: FormGroup;
    descricaoBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordernarPor = 'descricao';
    direcao = DirecaoOrder.ASC;
    grupoAtividades: Array<GrupoAtividade>;

    isEmEdicao = false;
    grupoAtividadeEmEdicao: GrupoAtividade;

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private grupoAtividadeService: GrupoAtividadeService) {

    }

    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Grupo Atividade';
        this.buscarDados();
        this.grupoAtividadeForm = this.fb.group({
            descricao: this.fb.control('', [Validators.required]),
            ordem: this.fb.control('', [Validators.required])

        });
    }

    salvar() {
        const id = this.grupoAtividadeForm.value.id;
        const descricao = this.grupoAtividadeForm.value.descricao;
        const ordem = this.grupoAtividadeForm.value.ordem;
        let isDadosValidos = true;
        let msg = '';

        if (!descricao || descricao === '') {
            this.grupoAtividadeForm.controls['descricao'].setErrors({'incorrect': true});
            msg = 'O Campo grupo Atividade é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!ordem || ordem === '') {
            this.grupoAtividadeForm.controls['ordem'].setErrors({'incorrect': true});
            msg = 'O Campo ordem é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;
        }

        const grupoAtividade = new GrupoAtividade(descricao, ordem);
        if (this.isEmEdicao) {
            grupoAtividade.id = this.grupoAtividadeEmEdicao.id;
            this.atualizar(grupoAtividade);
        } else {
            this.inserir(grupoAtividade);
        }

    }

    public inserir(grupoAtividade: GrupoAtividade): void {
        this.grupoAtividadeService.inserir(grupoAtividade).subscribe((msgRetorno: MensagemRetorno) => {
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro salvo com sucesso'
            });
            this.grupoAtividadeForm.reset();
            this.buscarDados();
        }, error => {
            this.exibirMsgErro('Erro ao salvar registro');
        });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {

        this.grupoAtividadeService.buscaPaginada(
            this.descricaoBusca,
            this.page,
            this.linhasPorPagina,
            this.ordernarPor,
            this.direcao)
            .subscribe((res: any) => {
                this.grupoAtividades = res.content;
                this.qtdLinhas = res.totalElements;
            });
    }

    public editar(gra: GrupoAtividade) {
        this.grupoAtividadeForm.controls['descricao'].setValue(gra.descricao);
        this.grupoAtividadeForm.controls['ordem'].setValue(gra.ordem);
        this.isEmEdicao = true;
        this.grupoAtividadeEmEdicao = gra;
    }

    public excluir(gra: GrupoAtividade) {
        this.grupoAtividadeService.excluir(gra.id)
            .subscribe((msg: MensagemRetorno) => {

                if (!msg.sucesso) {
                    this.exibirMsgErro(msg.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro excluido com sucesso'
                });

                this.grupoAtividades.splice(
                    this.grupoAtividades.indexOf(gra, 0), 1);
                this.qtdLinhas--;

            }, error1 => {
                this.exibirMsgErro('Erro ao tentar excluir registro');
            });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public exibirMsg(msg: MensagemRetorno): void {
        if (msg) {
            msg.msgList.forEach(element => {
                this.exibirMsgErro(element);
            });
        }
    }

    public resetarEdicao(): void {
        this.grupoAtividadeForm.reset();
        this.isEmEdicao = false;
        this.grupoAtividadeEmEdicao = null;
    }

    private atualizar(grupoAtividade: GrupoAtividade): void {
        this.grupoAtividadeService.editar(grupoAtividade)
            .subscribe((msgRetorno: MensagemRetorno) => {
                if (!msgRetorno.sucesso) {
                    this.exibirMsgErro(msgRetorno.msgList.toString());
                    return;
                }

                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro editado com sucesso'
                });

                this.buscarDados();
                this.resetarEdicao();
            }, error => {
                this.exibirMsgErro('Erro ao tentar editar o registro');
            });
    }

}
