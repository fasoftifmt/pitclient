import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_CONFIG} from '../../../config/api.config';
import {Observable} from 'rxjs';
import {DirecaoOrder} from '../../../models/enums/direcao-order.enum';
import {Disciplina} from '../../../models/disciplina.model';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';
import { DisciplinaDTO } from '../../../models/dtos/disciplina.dto';


@Injectable()
export class DisciplinaService {

  constructor(public http: HttpClient) {
  }

  inserir(disciplina: DisciplinaDTO): Observable<MensagemRetorno> {
    return this.http.post<MensagemRetorno>(`${API_CONFIG.baseUrl}/disciplinas/inserir`, disciplina);
  }

  public buscaPaginada(descricao: string, pagina: number, linhasPorPagina: number,
                       ordenarPor: string, direcao: DirecaoOrder): Observable<any> {

      let url = `${API_CONFIG.baseUrl}/disciplinas/pagina?descricao=${descricao}&page=${pagina}`;
      url += `&linesPerPage=${linhasPorPagina}&orderBy=${ordenarPor}&direction${direcao.toString()}`;

      return this.http.get<any>(url);
    }

    public editar(dis: DisciplinaDTO): Observable<MensagemRetorno> {

      const url = `${API_CONFIG.baseUrl}/disciplinas`;

      return this.http.put<MensagemRetorno>(url, dis);
    }

    public excluir(id: number): Observable<MensagemRetorno> {
      return this.http.delete<MensagemRetorno>(`${API_CONFIG.baseUrl}/disciplinas/${id}`);
    }
}
