import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {RelatoriosService} from '../../../../servicos/relatorios.service';

@Component({
    selector: 'pit-relatorio-servidores',
    templateUrl: './relatorio-servidores.component.html',
    styleUrls: ['./relatorio-servidores.component.scss']
})
export class RelatorioServidoresComponent implements OnInit {

    constructor(private relatoriosService: RelatoriosService) {
    }

    ngOnInit() {
    }

    gerar() {
        this.relatoriosService.relatorioDeServidor().then(value => {
            this.downLoadFile(value);
        });
    }

    downLoadFile(data: any) {
        const url = window.URL.createObjectURL(data);
        window.open(url);
    }
}
