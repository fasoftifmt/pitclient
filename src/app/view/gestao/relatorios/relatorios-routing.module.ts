import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatorioServidoresComponent} from './relatorio-servidores/relatorio-servidores.component';


const routes: Routes = [
    {
        path: 'sevidores',
        component: RelatorioServidoresComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }
