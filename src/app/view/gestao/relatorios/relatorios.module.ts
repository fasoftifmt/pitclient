import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../shared.module';
import {RelatoriosRoutingModule} from './relatorios-routing.module';
import {FormsModule} from '@angular/forms';
import {RelatorioServidoresComponent} from './relatorio-servidores/relatorio-servidores.component';

@NgModule({
    declarations: [RelatorioServidoresComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        RelatoriosRoutingModule
    ]
})
export class RelatoriosModule {
}
