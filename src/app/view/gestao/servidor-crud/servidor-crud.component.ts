import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'primeng/api';
import {MensagemRetorno} from '../../../models/mensagem-retorno.model';
import {Servidor} from 'src/app/models/servidor.model';
import {DirecaoOrder} from '../../../models/enums/direcao-order.enum';
import {ServidorService} from '../../../servicos/servidor.service';
import {RegimeTrabalho} from '../../../models/enums/regime-trabalho.model';
import {SituacaoServidor} from '../../../models/enums/SituacaoServidor';
import {TipoDeVinculo} from '../../../models/enums/tipo-de.vinculo';
import {DepartamentoServico} from '../../../servicos/departamento.service';
import {Departamento} from '../../../models/departamento.model';
import {UsuarioService} from '../../../servicos/usuario.service';
import {Usuario} from '../../../models/usuario.model';
import {PerfilUsuario, PerfilUsuarioModel} from '../../../models/enums/perfil-usuario.enum';
import {onErrorResumeNext} from 'rxjs';
//import {UsuarioCrudComponent} from './usuario-crud.component';


@Component({
    selector: 'pit-servidor-crud',
    templateUrl: './servidor-crud.component.html',
    styleUrls: ['./servidor-crud.component.scss'],
    providers: [DepartamentoServico, UsuarioService]
})
export class ServidorCrudComponent implements OnInit {
    servidorForm: FormGroup;
    nomeBusca = '';
    page = 0;
    qtdLinhas = 0;
    linhasPorPagina = 5;
    indexLinha = 0;
    ordenarPor = 'nome';
    direcao = DirecaoOrder.ASC;
    servidores: Array<Servidor>;

    isEmEdicao = false;
    servidorEmEdicao: Servidor;

    departamentoList: Array<Departamento>;
    perfilUsuarioList = PerfilUsuario.getValues();
    regimeTrabalhoList = RegimeTrabalho.getValues();
    tipoDeVinculoList = TipoDeVinculo.getValues();
    situacaoServidorList = SituacaoServidor.getValues();

    constructor(private fb: FormBuilder,
                private messageService: MessageService,
                private servidorService: ServidorService,
                private departamentoServico: DepartamentoServico,
                private usuarioService: UsuarioService) {
    }


    ngOnInit() {
        document.getElementById('titulo-gestao').innerText = 'Servidor';
        this.buscarDados();
        this.servidorForm = this.fb.group({
            nome: this.fb.control('', [Validators.required]),
            email: this.fb.control('', [Validators.required]),
            fone: this.fb.control('', [Validators.required]),
            matriculaSiape: this.fb.control('', [Validators.required]),
            regimeTrabalho: this.fb.control(null, [Validators.required]),
            tipoDeVinculo: this.fb.control(null, [Validators.required]),
            situacaoServidor: this.fb.control(null, [Validators.required]),
            departamento: this.fb.control(null, [Validators.required]),
            perfilUsuario: this.fb.control(null, [Validators.required])
        });
        this.listarDepartamentos();
    }


    salvar() {
        const nome = this.servidorForm.value.nome;
        const fone = this.servidorForm.value.fone;
        const email = this.servidorForm.value.email;
        const matriculaSiape = this.servidorForm.value.matriculaSiape;

        const regimeTrabalho = this.servidorForm.value.regimeTrabalho;
        const tipoDeVinculo = this.servidorForm.value.tipoDeVinculo;
        const situacaoServidor = this.servidorForm.value.situacaoServidor;

        const departamento = this.servidorForm.value.departamento;
        const idServidor = this.servidorForm.value.idServidor;

        // criação dos dados da instancia de Usuário
        const perfilUsuario = this.servidorForm.value.perfilUsuario;

        let isDadosValidos = true;
        let msg = '';
        if (!nome || nome === '') {
            this.servidorForm.controls['nome'].setErrors({'incorrect': true});
            msg = 'O campo nome é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!fone || fone === '') {
            this.servidorForm.controls['fone'].setErrors({'incorrect': true});
            msg = 'O campo telefone é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!email || email === '') {
            this.servidorForm.controls['email'].setErrors({'incorrect': true});
            msg = 'O campo e-mail é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!matriculaSiape || matriculaSiape === '') {
            this.servidorForm.controls['matriculaSiape'].setErrors({'incorrect': true});
            msg = 'O campo Matricula Siape é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!regimeTrabalho || regimeTrabalho === '') {
            this.servidorForm.controls['regimeTrabalho'].setErrors({'incorrect': true});
            msg = 'O campo Regime de Trabalho é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!tipoDeVinculo || tipoDeVinculo === '') {
            this.servidorForm.controls['tipoDeVinculo'].setErrors({'incorrect': true});
            msg = 'O campo tipo de vinculo é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if (!situacaoServidor || situacaoServidor === '') {
            this.servidorForm.controls['departamento'].setErrors({'incorrect': true});
            msg = 'O campo Situaçao Servidor é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if ( !departamento || departamento === ''){
            this.servidorForm.controls['situacaoServidor'].setErrors({'incorrect': true});
            msg = 'O campo Situaçao Servidor é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }
        if( !perfilUsuario || perfilUsuario === ''){
            this.servidorForm.controls['perfilUsuario'].setErrors({'incorrect': true});
            msg = 'O campo Perfil Usuário é obrigatório';
            this.exibirMsgErro(msg);
            isDadosValidos = false;
        }

        if (!isDadosValidos) {
            return;

        }
        const login = matriculaSiape;
        const senha = email;

        const usuario = this.criarUsuario(login, senha, perfilUsuario);

        console.log(usuario);

        const servidor = new Servidor(idServidor, nome, email, matriculaSiape,
        fone, departamento,  regimeTrabalho.getName(),
        tipoDeVinculo.getName(), situacaoServidor.getName());

        servidor.usuario = usuario;

        console.log(servidor)

        if (this.isEmEdicao) {
            servidor.idServidor = this.servidorEmEdicao.idServidor;
            this.atualizar(servidor);
        } else {
            this.inserir(servidor);
        }

    }
    listarDepartamentos(){
        this.departamentoServico
            .findAll()
            .subscribe( (departamentoList: Array<Departamento>) => {
                this.departamentoList = departamentoList;
            }, erro => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Erro',
                    detail: 'Erro ao buscar departamento'
                });
            });

    }

    criarUsuario(login: string, senha: string, perfilUsuario: PerfilUsuario): Usuario {
        const listSet = new Array<PerfilUsuarioModel>();
        // const p = PerfilUsuario.toEnum(perfilUsuario);
        listSet.push(perfilUsuario.getName());
        const usuario = new Usuario(undefined,login, senha);
        usuario.perfilUsuarioSet = listSet;
        console.log(usuario);
        return usuario;
        // this.inserirUsuario(usuario);
        // return this.recuperarUsuario(usuario.login);
    }

    public inserirUsuario(usuario: Usuario): void {
        this.usuarioService.inserir(usuario)
            .subscribe( (msgRetorno: MensagemRetorno) => {
                this.messageService.add( {
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
            }, error => {
                this.exibirMsgErro('Erro ao salvar registro do usuario');
            });
    }

    public recuperarUsuario(login: string): Usuario{
        this.usuarioService.getUsuarioPorLogin(login)
            .subscribe( (usuario: Usuario) => {
                return this.usuarioFromDTO(usuario);
            }, erro => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Erro',
                    detail: 'Erro ao buscar Usuario'
                });
            });
        return null;
    }

    usuarioFromDTO(u: Usuario): Usuario {
        const usuario = new Usuario(u.idUsuario, u.login, u.senha );
        usuario.perfilUsuarioSet = u.perfilUsuarioSet;
        return usuario;
    }

    public inserir(servidor: Servidor): void {
        this.servidorService.inserir(servidor)
            .subscribe((msgRetorno: MensagemRetorno) => {
                this.messageService.add({
                    severity: 'success',
                    summary: 'Sucesso',
                    detail: 'Registro salvo com sucesso'
                });
                this.servidorForm.reset();
                this.buscarDados();
            }, error => {
                this.exibirMsgErro('Erro ao salvar registro');
            });
    }

    paginar(event: any) {
        this.indexLinha = event.first;
        this.page = event.page;
        this.linhasPorPagina = event.rows;
        this.buscarDados();
    }

    public buscarDados() {
        this.servidorService.buscaPaginada(
            this.nomeBusca,
            this.page,
            this.linhasPorPagina,
            this.ordenarPor,
            this.direcao
        )
            .subscribe((res: any) => {
                this.servidores = res.content;
                this.servidores = this.servidorService.fromDTOList(this.servidores);
                this.qtdLinhas = res.totalElements;
            });
    }

    public editar(serv: Servidor) {
        this.servidorForm.controls['nome'].setValue(serv.nome);
        this.servidorForm.controls['fone'].setValue(serv.fone);
        this.servidorForm.controls['email'].setValue(serv.email);
        this.servidorForm.controls['matriculaSiape'].setValue(serv.matriculaSiape);
        this.servidorForm.controls['regimeTrabalho'].setValue(serv.regimeTrabalho);
        this.servidorForm.controls['tipoDeVinculo'].setValue(serv.tipoDeVinculo);
        this.servidorForm.controls['situacaoServidor'].setValue(serv.situacaoServidor);
        this.servidorForm.controls['departamento'].setValue( this.departamentoList
            .find(value => value.id === serv.departamentoArea.id));
        console.log(serv)
        this.isEmEdicao = true;
        this.servidorEmEdicao = serv;
    }

    public excluir(serv: Servidor) {
        this.servidorService
            .excluir(serv.idServidor)
            .subscribe((msg: MensagemRetorno) => {
            if (!msg.sucesso) {
                this.exibirMsgErro(msg.msgList.toString());
                return;
            }
            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro excluido com sucesso'
            });
            this.servidores.splice(
                this.servidores.indexOf(serv, 0), 1);
            this.qtdLinhas--;
        }, error => {
            this.exibirMsgErro('Erro ao tentar excluir registro');
        });
    }

    public exibirMsgErro(msg: string): void {
        this.messageService.add({
            severity: 'error',
            summary: 'Erro',
            detail: msg
        });
    }

    public atualizar(servidor: Servidor): void {
        this.servidorService
            .editar(servidor).subscribe((msgRetorno: MensagemRetorno) => {
            if (!msgRetorno.sucesso) {
                this.exibirMsgErro(msgRetorno.msgList.toString());
                return;
            }

            this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Registro Alterado com sucesso'
            });

            this.buscarDados();
            this.resetarEdicao();
        }, error => {
            this.exibirMsgErro('Erro a tentar atualizar o registro');
        });

    }

    public resetarEdicao(): void {
        this.servidorForm.reset();
        this.isEmEdicao = false;
        this.servidorEmEdicao = null;
    }
}
