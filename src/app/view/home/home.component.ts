import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {PlanoDeTrabalhoService} from '../../servicos/plano-de-trabalho.service';
import {PlanoDeTrabalhoResumido} from '../../models/plano-de-trabalho-resumido.model';
import {UsuarioService} from '../../servicos/usuario.service';
import {StatusPtd} from '../../models/enums/status-ptd.enum';
import {PitUtil} from '../../utils/plano-de-trabalho.util';
import {Servidor} from '../../models/servidor.model';

@Component({
    selector: 'pit-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    


    public listaPIT: Array<PlanoDeTrabalhoResumido>;

    constructor(private messageService: MessageService,
                private planoDeTrabalhoService: PlanoDeTrabalhoService,
                private usuarioService: UsuarioService) {
    }

    ngOnInit() {
        this.buscarUltimosPlanosDeTrabalho();
    }

    public statusToString(statusPtd: StatusPtd): string {
        return PitUtil.statusPITToString(statusPtd);
    }

    private buscarUltimosPlanosDeTrabalho() {

        this.usuarioService.getServidorAtual()
            .then((servidor: Servidor) => {
                this.planoDeTrabalhoService.buscarPorDocente(servidor.idServidor)
                    .subscribe((pitResumidoList: PlanoDeTrabalhoResumido[]) => {
                        this.listaPIT = pitResumidoList;
                    }, error1 => {
                        console.log(error1);
                    });
            });

        }
}
