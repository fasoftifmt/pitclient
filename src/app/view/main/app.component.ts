import {Component} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';

@Component({
    selector: 'pit-root',
    templateUrl: './app.component.html',
    styleUrls: []
})
export class AppComponent {

    esconderCabecalhoERodape = false;

    constructor(private router: Router) {
        router.events.forEach((event) => {
            if (event instanceof NavigationStart) {
                const url: string = event['url'];
                this.esconderCabecalhoERodape = !url.includes('/login', 0);
            }
        });
    }
}
