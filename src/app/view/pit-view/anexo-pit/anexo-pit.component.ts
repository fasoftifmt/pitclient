import {Component, Input, OnInit} from '@angular/core';
import {PlanoDeTrabalho} from '../../../models/plano-de-trabalho.model';
import {AnexoPit} from '../../../models/anexo-pit.model';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'pit-anexo',
    templateUrl: './anexo-pit.component.html',
    styleUrls: ['./anexo-pit.component.scss']
})
export class AnexoPitComponent implements OnInit {

    @Input() planoDeTrabalho: PlanoDeTrabalho;

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {
    }

    public selecionarArquivo(inputFile: HTMLInputElement, label: HTMLLabelElement): void {
        label.innerHTML = inputFile.files[0].name;
    }

    public adicionarAnexo(labelNomeArquivo: HTMLLabelElement, inputComplemento: HTMLInputElement, inputFile: HTMLInputElement) {
        if (this.carregarEValidarArquivo(inputFile, inputComplemento)) {
            labelNomeArquivo.innerHTML = '';
            inputComplemento.value = '';
        }
    }

    public removerAnexo(anexo: AnexoPit) {
        this.planoDeTrabalho.anexoPITList.splice(
            this.planoDeTrabalho.anexoPITList.indexOf(anexo, 0), 1);
    }

    private carregarEValidarArquivo(input: HTMLInputElement, inputComplemento: HTMLInputElement): boolean {
        const reader = new FileReader();
        const desc = inputComplemento.value;
        const files = input.files;
        if (files && files.length > 0) {
            const file = files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                if (file.size > 1500000) {
                    const msg = 'Documento Maior que o permitido (1,5 Mega): ' + file.size;
                    this.messageService.add({
                        severity: 'warn',
                        summary: 'Dado Inválido!',
                        detail: msg
                    });
                } else {
                    const anexo = new AnexoPit(file.name, desc, file.type, reader.result.toString().split(',')[1]);
                    this.planoDeTrabalho.anexoPITList.push(anexo);
                }
            };
            return true;
        } else {
            this.messageService.add({
                severity: 'warn',
                summary: 'Dado Inválido!',
                detail: `Selecione um arquivo`
            });
            return false;
        }
    }
}
