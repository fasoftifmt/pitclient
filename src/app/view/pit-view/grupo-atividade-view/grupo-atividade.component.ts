import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {GrupoAtividade} from '../../../models/grupo-atividade.model';
import {Atividade} from '../../../models/atividade.model';
import {MessageService} from 'primeng/api';
import {NumberUtil} from '../../../utils/number.util';

@Component({
    selector: 'pit-grupo-atividade',
    templateUrl: './grupo-atividade.component.html',
    styleUrls: ['./grupo-atividade.component.scss']
})
export class GrupoAtividadeComponent implements OnInit, OnChanges {

    @Input() grupoAtividade: GrupoAtividade;
    @Output() public atualizarCargaHorariaGeral: EventEmitter<any> = new EventEmitter<any>();
    public grupoAtividadeAtivo: GrupoAtividade;
    public subAtividades = new Array<Atividade>();

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        this.grupoAtividadeAtivo = this.grupoAtividade;
        this.atualizarListaSubAtividades(this.grupoAtividadeAtivo);
    }

    public atualizarCargaHoraria(input: HTMLInputElement, atividade: Atividade): void {
        atividade.quantidade = Number((input.value !== '' ? input.value : 0));
        if (!this.realizarAtualizacaoCargaHoraria(atividade)) {
            input.value = '0';
            atividade.cargaHorariaObtida = 0;
            atividade.quantidade = 0;
        }
        this.atualizarSubAtividades();
        this.atualizarCargaHorariaGeral.emit();
    }

    public atualizaLancamentoAtividade(input: HTMLSelectElement, atividade: Atividade): void {
        atividade.fator = Number(input.value);
        atividade.qtdLancamento = atividade.fator;
        if (!this.realizarAtualizacaoCargaHoraria(atividade)) {
            atividade.cargaHorariaObtida = 0;
            atividade.quantidade = 0;
        }
        this.atualizarSubAtividades();
        this.atualizarCargaHorariaGeral.emit();
    }

    public atualizarFatorAtividade(inputFator: HTMLInputElement, atividade: Atividade): void {
        const valor = inputFator.value;
        if (valor === undefined || valor === '' || Number(valor) < 0) {
            this.messageService.add({
                severity: 'warn',
                summary: 'Valor Inválido!',
                detail: `O fator informado não é válido`
            });
            return;
        }

        atividade.fator = Number(valor);
        atividade.cargaHorariaObtida = NumberUtil.arredondar(atividade.fator * atividade.quantidade);
        this.atualizarCargaHorariaGeral.emit();
    }

    private realizarAtualizacaoCargaHoraria(atividade: Atividade): boolean {
        if (atividade.quantidade !== undefined && atividade.quantidade >= 0) {
            atividade.cargaHorariaObtida = NumberUtil.arredondar(atividade.fator * atividade.quantidade);
            if (atividade.cargaHorariaObtida > atividade.qtdMax) {
                this.messageService.add({
                    severity: 'warn',
                    summary: 'Valor Inválido!',
                    detail: `A quantidade Máxima é ${atividade.qtdMax}`
                });
                return false;
            }
        }
        return true;
    }

    private atualizarSubAtividades(): void {
        const atividadesComSubAtividades = this.grupoAtividadeAtivo.getAtividadesComSubAtividades();
        if (atividadesComSubAtividades.length > 0) {
            this.zerarValoresSubAtividade();

            atividadesComSubAtividades.forEach(atividade => {
                atividade.subAtividadeList.forEach(subAtividade => {
                    this.subAtividades.forEach(subList => {
                        if (subList.id === subAtividade.id && atividade.cargaHorariaObtida !== null) {
                            subList.quantidade = NumberUtil.arredondar(subList.quantidade + atividade.cargaHorariaObtida);
                            subList.cargaHorariaObtida = NumberUtil.arredondar(subList.fator * subList.quantidade);
                        }
                    });
                });
            });
        }
    }

    private zerarValoresSubAtividade(): void {
        this.subAtividades.forEach(subList => {
            subList.quantidade = 0;
            subList.cargaHorariaObtida = 0;
        });
    }

    private atualizarListaSubAtividades(grupoAtividadeAtivo: GrupoAtividade) {
        grupoAtividadeAtivo.categoriaAtividadeList.forEach(value => {
            if (value.contemSubAtividade()) {
                value.getAtividadesList().forEach(subAtividade => {
                    const find = this.subAtividades.find((atv: Atividade) => atv.id === subAtividade.id);
                    if (subAtividade.isSubAtividade && find === undefined) {
                        subAtividade.quantidade = subAtividade.quantidade === undefined ? 0 : subAtividade.quantidade;
                        subAtividade.cargaHorariaObtida =
                            subAtividade.cargaHorariaObtida === undefined ? 0 : subAtividade.cargaHorariaObtida;
                        this.subAtividades.push(subAtividade);
                    }
                });
            }
        });
    }
}
