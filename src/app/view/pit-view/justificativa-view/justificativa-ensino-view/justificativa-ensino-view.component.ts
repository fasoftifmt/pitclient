import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {CursoService} from '../../../../servicos/curso.service';
import {Curso} from '../../../../models/curso.model';
import {Disciplina} from '../../../../models/disciplina.model';
import {PlanoDeTrabalho} from '../../../../models/plano-de-trabalho.model';
import {JustificativaEnsino} from '../../../../models/justificativa-ensino.model';
import {MessageService} from 'primeng/api';

@Component({
    selector: 'pit-justificativa-ensino-view',
    templateUrl: './justificativa-ensino-view.component.html',
    styleUrls: ['./justificativa-ensino-view.component.scss']
})
export class JustificativaEnsinoViewComponent implements OnInit, OnChanges {

    @Input() planoDeTrabalho: PlanoDeTrabalho;
    @Input() public cursos: Curso[];
    public disciplinas: Array<Disciplina>;

    public formulario: FormGroup = new FormGroup({
        'inputCurso': new FormControl(),
        'inputDisciplina': new FormControl(),
        'inputTurma': new FormControl(),
        'inputAulas': new FormControl()
    });

    constructor(private cursoService: CursoService, private messageService: MessageService) {
    }

    ngOnChanges(): void {
        if (this.cursos) {
            this.disciplinas = this.cursos[0].disciplinas;
        }
    }

    ngOnInit() {
    }

    public removerAtividade(justificativaEnsino: JustificativaEnsino) {
        this.planoDeTrabalho.justificativaEnsinoList.splice(
            this.planoDeTrabalho.justificativaEnsinoList.indexOf(justificativaEnsino, 0), 1);
    }

    public atualizaDisciplinas() {
        this.disciplinas = (this.getCursoPorId(Number(this.formulario.value.inputCurso))).disciplinas;
    }

    public adicionarAtividadeEnsino() {
        const curso = this.formulario.value.inputCurso;
        const disciplina = this.formulario.value.inputDisciplina;
        const turma = this.formulario.value.inputTurma;
        const aulas = this.formulario.value.inputAulas;

        if (curso === null || disciplina === null || turma === '' || turma === null || aulas === '' || aulas === null) {
            this.messageService.add({
                severity: 'warn',
                summary: 'Campos Inválidos!',
                detail: `Preencha todos os campos`
            });
            return;
        }
        const cursoEncontrado = this.getCursoPorId(Number(curso));

        const atv = new JustificativaEnsino(null, cursoEncontrado.descricao, this.getDisciplinaPorId(Number(disciplina)), turma, aulas);

        this.planoDeTrabalho.justificativaEnsinoList.push(atv);

        this.formulario.get('inputTurma').setValue('');
        this.formulario.get('inputAulas').setValue('');
    }

    public getCursoPorId(cursoId: number): Curso {
        return this.cursos.find((curso) => {
            return curso.id === cursoId;
        });
    }

    public getDisciplinaPorId(disciplinaId: number): Disciplina {
        return this.disciplinas.find((disciplina) => {
            return disciplina.id === disciplinaId;
        });
    }
}
