import {Component, Input, OnInit} from '@angular/core';
import {PlanoDeTrabalho} from '../../../models/plano-de-trabalho.model';
import {Curso} from '../../../models/curso.model';

@Component({
    selector: 'pit-justificativa-view',
    templateUrl: './justificativa-view.component.html',
    styleUrls: ['./justificativa-view.component.scss']
})
export class JustificativaViewComponent implements OnInit {

    @Input() planoDeTrabalho: PlanoDeTrabalho;
    @Input() public cursos: Curso[];

    constructor() {
    }

    ngOnInit() {
    }

}
