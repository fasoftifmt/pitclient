import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PlanoDeTrabalhoService} from '../../servicos/plano-de-trabalho.service';
import {PlanoDeTrabalho} from '../../models/plano-de-trabalho.model';
import {GrupoAtividade} from '../../models/grupo-atividade.model';
import {StatusPtd} from '../../models/enums/status-ptd.enum';
import {UsuarioService} from '../../servicos/usuario.service';
import {GrupoCategoriaService} from '../../servicos/grupo-categoria.service';
import {AtividadePit} from '../../models/atividade-pit.model';
import {HttpResponse} from '@angular/common/http';
import {MessageService} from 'primeng/api';
import {Servidor} from '../../models/servidor.model';
import {RegimeTrabalho, RegimeTrabalhoModel} from '../../models/enums/regime-trabalho.model';
import {NumberUtil} from '../../utils/number.util';
import {JustificativaEnsino} from '../../models/justificativa-ensino.model';
import {Painel} from '../../models/enums/painel-visivel.enum';
import {AnexoPit} from '../../models/anexo-pit.model';
import {CursoService} from '../../servicos/curso.service';
import {Curso} from '../../models/curso.model';
import {GrupoAtividadeDTO} from '../../models/dtos/grupo-atividade.dto';

@Component({
    selector: 'pit-pit-view',
    templateUrl: './pit-view.component.html',
    styleUrls: ['./pit-view.component.scss'],
    providers: [CursoService]
})
export class PitViewComponent implements OnInit {

    public planoDeTrabalho: PlanoDeTrabalho;
    public grupoAtividadeList: Array<GrupoAtividade>;
    public grupoAtividadeAtivo: GrupoAtividade;
    public cargaHorariaTotalGeral = 0;
    public painelVisivel = Painel.SERVIDOR;
    public usuarioAtual: Servidor;
    public cursos: Curso[];
    private param: string;

    constructor(
        private router: ActivatedRoute,
        private routerRoot: Router,
        private pitService: PlanoDeTrabalhoService,
        private usuarioService: UsuarioService,
        private grupoCategoriaService: GrupoCategoriaService,
        private messageService: MessageService,
        private cursoService: CursoService) {
    }

    ngOnInit() {
        this.usuarioService.getServidorAtual()
            .then((servidor: Servidor) => {
                this.usuarioAtual = servidor;
            });

        this.router.params.subscribe((param: Params) => {
            this.param = param.estado;
        });

        if (this.param === 'novo') {
            this.iniciarNovoPlanoDeTrabalho();
            this.cursoService.findAll()
                .subscribe(curso => {
                    this.cursos = curso;
                });
        }
    }

    public exibirServidorPainel(): void {
        this.painelVisivel = Painel.SERVIDOR;
    }

    public enviarPlanoTrabalho() {
        if (!this.isCargaHorariaCompleta()) {
            this.messageService.add({
                severity: 'warn',
                summary: 'Carga Horária incompleta!',
                detail: `A carga horária obtida não é válida para seu Regime de trabalho`
            });
            return;
        }
        const atvPitList = new Array<AtividadePit>();

        this.grupoAtividadeList.forEach((grupo) => {
            grupo.categoriaAtividadeList.forEach((cat) => {
                cat.getAtividadesList().forEach((atv) => {
                    if (atv.quantidade > 0) {
                        const atvPit = new AtividadePit(null, atv.quantidade, atv, atv.qtdLancamento, atv.fator);
                        atvPitList.push(atvPit);
                    }
                });
            });
        });

        this.planoDeTrabalho.atividadePitList = atvPitList;
        this.planoDeTrabalho.gruposAtividades = null;
        this.planoDeTrabalho.dataElaboracao = this.formataData(new Date());


        console.log(JSON.stringify(this.planoDeTrabalho));

        this.pitService.insert(this.planoDeTrabalho)
            .subscribe((response: HttpResponse<any>) => {
                    this.messageService.add({severity: 'success', summary: 'Sucesso', detail: 'PIT Enviado com sucesso!'});
                    this.routerRoot.navigate(['']);
                },
                (erro) => {
                    console.log(erro);
                });
    }

    public formataData(data: Date): string {
        let dataFormatada = data.toLocaleString('pt-br').replace(/[/]/, '-');
        dataFormatada = dataFormatada.replace(/[/]/, '-');
        return dataFormatada;
    }

    public atualizarCargaHorariaGeral() {
        this.cargaHorariaTotalGeral = 0;
        this.grupoAtividadeList.forEach((grupo) => {
            this.cargaHorariaTotalGeral += grupo.getCargaHorariaTotal();
            this.cargaHorariaTotalGeral = NumberUtil.arredondar(this.cargaHorariaTotalGeral);
        });
    }

    public isCargaHorariaCompleta(): boolean {
        if (this.usuarioAtual.regimeTrabalho + '' == RegimeTrabalhoModel.QUARENTA_HORAS.toString()) {
            return this.cargaHorariaTotalGeral === 40;
        }
        return this.cargaHorariaTotalGeral === 20;
    }

    public exibirJustificativa() {
        this.painelVisivel = Painel.JUSTIFICATIVA;
    }

    public exibirAnexo() {
        this.painelVisivel = Painel.ANEXO;
    }

    private alterarGrupo(grupoAtividade: GrupoAtividade) {
        this.painelVisivel = Painel.ATIVIDADES;
        this.grupoAtividadeAtivo = grupoAtividade;
    }

    private iniciarNovoPlanoDeTrabalho() {

        this.usuarioService.getServidorAtual()
            .then((servidor: Servidor) => {

                const planoDeTrabalho = new PlanoDeTrabalho(
                    null,
                    null,
                    StatusPtd.ENVIADO,
                    servidor
                );

                planoDeTrabalho.anexoPITList = new Array<AnexoPit>();
                planoDeTrabalho.justificativaEnsinoList = new Array<JustificativaEnsino>();

                this.grupoCategoriaService.findAll().subscribe((group: GrupoAtividadeDTO[]) => {

                    this.grupoAtividadeList = this.grupoCategoriaService.fromDTOList(group);
                    this.grupoAtividadeAtivo = this.grupoAtividadeList[0];
                    this.planoDeTrabalho = planoDeTrabalho;

                }, erro => {
                    console.log(erro);
                });
            });
    }
}
