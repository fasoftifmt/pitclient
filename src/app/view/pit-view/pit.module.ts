import {NgModule} from '@angular/core';
import {PitViewComponent} from './pit-view.component';
import {GrupoAtividadeComponent} from './grupo-atividade-view/grupo-atividade.component';
import {ServidorViewComponent} from './servidor-view/servidor-view.component';
import {JustificativaViewComponent} from './justificativa-view/justificativa-view.component';
import {JustificativaEnsinoViewComponent} from './justificativa-view/justificativa-ensino-view/justificativa-ensino-view.component';
import {AnexoPitComponent} from './anexo-pit/anexo-pit.component';
import {GrupoCategoriaService} from '../../servicos/grupo-categoria.service';
import {CategoriaService} from '../../servicos/categoria.service';
import {AtividadeService} from '../../servicos/atividade.service';
import {RouterModule, Routes} from '@angular/router';
import {LoadingComponent} from '../loading/loading.component';
import {SharedModule} from '../../shared.module';
import {CommonModule} from '@angular/common';
import {RelatoriosService} from '../../servicos/relatorios.service';
import { RegistroHistoricoViewComponent } from './registro-historico-view/registro-historico-view.component';

const ROUTES: Routes = [
    {path: '', component: PitViewComponent}
];

@NgModule({
    declarations: [
        PitViewComponent,
        ServidorViewComponent,
        JustificativaViewComponent,
        JustificativaEnsinoViewComponent,
        AnexoPitComponent,
        LoadingComponent,
        RegistroHistoricoViewComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(ROUTES)
    ],
    providers: [
        GrupoCategoriaService,
        CategoriaService,
        AtividadeService,
        RelatoriosService
    ]
})
export class PitModule {
}
