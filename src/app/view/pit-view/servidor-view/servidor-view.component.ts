import {Component, OnInit} from '@angular/core';
import {Servidor} from '../../../models/servidor.model';
import {UsuarioService} from '../../../servicos/usuario.service';

@Component({
    selector: 'pit-servidor-view',
    templateUrl: './servidor-view.component.html',
    styleUrls: ['./servidor-view.component.scss']
})
export class ServidorViewComponent implements OnInit {

    public usuario: Servidor;

    constructor(private usuarioService: UsuarioService) {
    }

    ngOnInit() {
        this.usuarioService.getServidorAtual()
            .then((servidor: Servidor) => {
                this.usuario = servidor;
            });
    }
}
