import {AfterContentInit, Component, ContentChild, Input, OnInit} from '@angular/core';
import {FormControlName, NgModel} from '@angular/forms';

@Component({
    selector: 'pit-input-container',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, AfterContentInit {

    @Input() errorMessage: string;
    @Input() showTip = true;

    input: any;

    @ContentChild(NgModel, null) model: NgModel;
    @ContentChild(FormControlName, null) control: FormControlName;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterContentInit() {
        this.input = this.model || this.control;
        if (this.input === undefined) {
            throw new Error('Esse componente precisa ser usado com uma diretiva ngModel ou formControlName');
        }
    }

    hasSuccess(): boolean {
        return this.input.valid && (this.input.dirty || this.input.touched);
    }

    hasError(): boolean {
        return this.input.invalid && (this.input.dirty || this.input.touched);
    }
}
