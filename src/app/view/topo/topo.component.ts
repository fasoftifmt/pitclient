import {Component, OnInit} from '@angular/core';
import {UsuarioService} from '../../servicos/usuario.service';

@Component({
    selector: 'pit-topo',
    templateUrl: './topo.component.html',
    styleUrls: ['./topo.component.scss']
})
export class TopoComponent implements OnInit {

    constructor(private usuarioService: UsuarioService) {
    }

    ngOnInit() {
    }

    public sair(): void {
        this.usuarioService.logout();
    }

    irParaHome() {
        this.usuarioService.redirecionarParaInicio();
    }
}
