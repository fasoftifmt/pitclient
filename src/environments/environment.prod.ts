export const environment = {
    production: true,
    homologacao: false,
    url_api: 'http://israeldev.ddns.net:80',
    url_gravatar: 'https://pt.gravatar.com'
};
