export const environment = {
    production: false,
    homologacao: false,
    url_api: 'http://localhost:8081',
    url_gravatar: 'https://pt.gravatar.com'
};
