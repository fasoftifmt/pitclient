export const environment = {
    production: false,
    homologacao: false,
    url_api: 'http://localhost:8080',
    url_gravatar: 'https://pt.gravatar.com'
};
